const express = require("express");
const mongoose = require("mongoose");
const dotenv = require("dotenv");
const cors = require("cors");
const cookieParser = require("cookie-parser");
// IMPORT ROUTERS
const { customerRouter } = require("./app/routers/customer.router");
const { orderRouter } = require("./app/routers/order.router");
const { orderDetailRouter } = require("./app/routers/orderDetail.router");
const { productRouter } = require("./app/routers/product.router");
const { productTypeRouter } = require("./app/routers/productType.router");
const { voucherRouter } = require("./app/routers/voucher.router");
const { userRouter } = require("./app/routers/user.router");
const app = express();

const corsConfig = {
    origin: [`http://localhost:3000`, `https://localhost:3000`, `http://127.0.0.1:3000`, `https://127.0.0.1:3000`],
    credentials: true,
    exposedHeaders: ["set-cookie"]
}
app.use(cors(corsConfig));
// CONFIG DOTENV
dotenv.config();
// CONFIG JSON
app.use(express.json());
// CONFIG COOKIE-PARSER
app.use(cookieParser());

// CONNECT TO MONGODB
mongoose.set('strictQuery', true);
mongoose.connect(process.env.MONGODB_URL, err => {
    if (err) throw err;
    console.log("Connect to MongoDB successful");
})

// APPLY ROUTERS
app.use("/", productTypeRouter);
app.use("/", productRouter);
app.use("/", customerRouter);
app.use("/", orderRouter);
app.use("/", orderDetailRouter);
app.use("/", voucherRouter);
app.use("/", userRouter);

// RUN APP ON PORT 8000
const port = 8000;
app.listen(port, () => {
    console.log("App listening on port:", port);
})