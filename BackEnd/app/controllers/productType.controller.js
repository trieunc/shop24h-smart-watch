const mongoose = require("mongoose");
const productTypeModel = require("../models/productType.model");

// create product type:
const createProductType = (req, res) => {
    // B1: Thu thập dữ liệu
    let body = req.body;
    // B2: Kiểm tra dữ liệu
    // check name
    if ( body.name === undefined || body.name.trim() === "" ) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Name is invalid"
        });
    }
    // check description
    if ( body.description === undefined || body.description.trim() === "" ) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Description is invalid"
        });
    }
    // B3: Gọi model và xử lý CSDL
    let newProductType = {
        name: body.name,
        description: body.description
    };
    productTypeModel.create(newProductType, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Server Internal Error",
                message: err.message
            });
        }
        else {
            return res.status(201).json({
                status: "Create Product Type successful",
                data
            });
        }
    });
}

// get all product type:
const getAllProductType = (req, res) => {
    // B1: Thu thập dữ liệu
    // B2: Kiểm tra dữ liệu
    // B3: Gọi model và xử lý CSDL
    productTypeModel.find((err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Server Internal Error",
                message: err.message
            });
        }
        else {
            return res.status(200).json({
                status: "Get All Product Types successful",
                data
            });
        }
    });
}

// get product type by ID:
const getProductTypeById = (req, res) => {
    // B1: Thu thập dữ liệu
    let productTypeId = req.params.typeId;
    // B2: Kiểm tra dữ liệu
    if ( !mongoose.Types.ObjectId.isValid(productTypeId) ) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Product-Type ID is invalid"
        });
    }
    // B3: Gọi model và xử lý CSDL
    productTypeModel.findById(productTypeId, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Server Internal Error",
                message: err.message
            });
        }
        else {
            return res.status(200).json({
                status: "Get Product-Type Detail successful",
                data
            });
        }
    });
}

// update product type by ID:
const updateProductType = (req, res) => {
    // B1: Thu thập dữ liệu
    let productTypeId = req.params.typeId;
    let body = req.body;
    // B2: Kiểm tra dữ liệu
    // check Product-Type ID
    if ( !mongoose.Types.ObjectId.isValid(productTypeId) ) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Product-Type ID is invalid"
        });
    }
    // check if name exist and name not valid
    if ( body.name !== undefined && body.name.trim() === "" ) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Name is invalid"
        });
    }
    // check if description exist and description not valid
    if ( body.description !== undefined && body.description.trim() === "" ) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Description is invalid"
        });
    }
    // B3: Gọi model và xử lý CSDL
    let updateProductType = {
        name: body.name,
        description: body.description
    }
    productTypeModel.findByIdAndUpdate(productTypeId, updateProductType, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Server Internal Error",
                message: err.message
            });
        }
        else {
            return res.status(200).json({
                status: "Update Product-Type successful",
                data
            });
        }
    })
}

// delete product type by ID:
const deleteProductType = (req, res) => {
    // B1: Thu thập dữ liệu
    let productTypeId = req.params.typeId;
    // B2: Kiểm tra dữ liệu
     // check Product-Type ID
     if ( !mongoose.Types.ObjectId.isValid(productTypeId) ) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Product-Type ID is invalid"
        });
    }
    // B3: Gọi model và xử lý CSDL
    productTypeModel.findByIdAndDelete(productTypeId, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Server Internal Error",
                message: err.message
            });
        }
        else {
            return res.status(204).json();
        }
    })
}

module.exports = { createProductType, getAllProductType, getProductTypeById, updateProductType, deleteProductType };