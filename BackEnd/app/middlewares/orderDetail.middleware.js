const orderDetailMiddleware = (req, res, next) => {
    console.log("ORDER-DETAIL MIDDLEWARE - TIME: " + new Date().toLocaleString() + " - METHOD: " + req.method + " - URL: " + req.url);
    next();
}

module.exports = { orderDetailMiddleware }