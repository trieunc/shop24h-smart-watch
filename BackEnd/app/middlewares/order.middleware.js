const orderMiddleware = (req, res, next) => {
    console.log("ORDER MIDDLEWARE - TIME: " + new Date().toLocaleString() + " - METHOD: " + req.method + " - URL: " + req.url);
    next();
}

module.exports = { orderMiddleware };