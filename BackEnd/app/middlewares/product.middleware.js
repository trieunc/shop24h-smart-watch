const productMiddleware = (req, res, next) => {
    console.log("PRODUCT MIDDLEWARE - TIME: " + new Date().toLocaleString() + " - METHOD: " + req.method + " - URL: " + req.url);
    next();
}

module.exports = { productMiddleware };