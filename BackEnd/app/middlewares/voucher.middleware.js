const voucherMiddleware = (req, res, next) => {
    console.log("VOUCHER MIDDLEWARE - TIME: " + new Date().toLocaleString() + " - METHOD: " + req.method + " - URL: " + req.url);
    next();
}

module.exports = { voucherMiddleware };