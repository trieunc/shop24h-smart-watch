const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const productTypeSchema = new Schema({
    name: {
        type: String,
        unique: true,
        required: true
    },
    description: {
        type: String,
        required: true
    }
}, {
    timestamps: true
});

module.exports = mongoose.model("product_type", productTypeSchema);