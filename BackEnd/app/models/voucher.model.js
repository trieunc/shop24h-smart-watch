const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const voucherSchema = new Schema({
    voucherCode: {
        type: String,
        required: true
    },
    discountPercent: {
        type: Number,
        required: true
    },
    note: {
        type: String
    }
}, {
    timestamps: true
});

module.exports = mongoose.model("voucher", voucherSchema);