const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const customerSchema = new Schema({
    fullName: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    profilePic: {
        type: String,
        default: "https://live.staticflickr.com/65535/52883722957_b739da5be8.jpg"
    },
    address: {
        type: String,
    },
    city: {
        type: String,
    },
    district: {
        type: String,
    },
    ward: {
        type: String,
    },
    user: {
        type: mongoose.Types.ObjectId,
        ref: "user"
    },
    orders: [{
        type: mongoose.Types.ObjectId,
        ref: "order"
    }]
}, {
    timestamps: true
})

module.exports = mongoose.model("customer", customerSchema);