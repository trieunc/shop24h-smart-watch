const express = require("express");
const { createProduct, getAllProducts, getProductById, updateProduct, deleteProduct } = require("../controllers/product.controller");
// import middlewares
const { productMiddleware } = require("../middlewares/product.middleware");
const productRouter = express.Router();

// router create product
productRouter.post("/products", productMiddleware, createProduct);
// router get all products: 
productRouter.get("/products", productMiddleware, getAllProducts);
// router get product detail by ID
productRouter.get("/products/:productId", productMiddleware, getProductById);
// router update product by ID
productRouter.put("/products/:productId", productMiddleware, updateProduct);
// router update product by ID
productRouter.delete("/products/:productId", productMiddleware, deleteProduct);

module.exports = { productRouter }