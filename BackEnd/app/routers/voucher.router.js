const express = require("express");
const { voucherMiddleware } = require("../middlewares/voucher.middleware");
const voucherRouter = express.Router();

// router create voucher
voucherRouter.post("/vouchers", voucherMiddleware);
// router get all vouchers: 
voucherRouter.get("/vouchers", voucherMiddleware);
// router get voucher detail by ID
voucherRouter.get("/vouchers/:voucherId", voucherMiddleware);
// router update voucher by ID
voucherRouter.put("/vouchers/:voucherId", voucherMiddleware);
// router update voucher by ID
voucherRouter.delete("/vouchers/:voucherId", voucherMiddleware);

module.exports = { voucherRouter };