const express = require("express");
const { getAllCustomers, getCustomerDetailById, updateCustomer, deleteCustomer, createNewCustomer, getCustomerInfo } = require("../controllers/customer.controller");
const customerRouter = express.Router();
const { customerMiddleware } = require("../middlewares/customer.middleware");
const userMiddleware = require("../middlewares/user.middleware");

// create customer
customerRouter.post("/customers", customerMiddleware, createNewCustomer);

// get all customers: 
customerRouter.get("/customers", userMiddleware.verifyTokenAndAdminRole, getAllCustomers);

// get customer infor by username or email:
customerRouter.get("/admin", userMiddleware.verifyTokenAndAdminRole, getCustomerInfo)

// get customer detail by ID
customerRouter.get("/customers/:customerId", userMiddleware.verifyTokenAndAdminRole, getCustomerDetailById);

// update customer by ID
customerRouter.put("/customers/:customerId", userMiddleware.verifyTokenAndAdminRole, updateCustomer);

// update customer by ID
customerRouter.delete("/customers/:customerId", userMiddleware.verifyTokenAndAdminRole, deleteCustomer);

module.exports = { customerRouter }