const express = require("express");
const { createAnOrderDetailOfOrder, getAllOrderDetailOfAnOrder, getAnOrderDetailById, updateOrderDetail, deleteOrderDetail } = require("../controllers/orderDetail.controller");
const { orderDetailMiddleware } = require("../middlewares/orderDetail.middleware");
const userMiddleware = require("../middlewares/user.middleware");


const orderDetailRouter = express.Router();

// router create order detail of an order
orderDetailRouter.post("/orders/:orderId/details", userMiddleware.verifyToken, createAnOrderDetailOfOrder);

// router get all orderDetail of one customer: 
orderDetailRouter.get("/orders/:orderId/details", userMiddleware.verifyToken, getAllOrderDetailOfAnOrder);

// router get orderDetail by ID
orderDetailRouter.get("/details/:detailId", userMiddleware.verifyTokenAndAdminRole, getAnOrderDetailById);

// router update orderDetail by ID
orderDetailRouter.put("/details/:detailId", userMiddleware.verifyTokenAndAdminRole, updateOrderDetail);

// router delete orderDetail by ID
orderDetailRouter.delete("/orders/:orderId/details/:detailId", userMiddleware.verifyTokenAndAdminRole, deleteOrderDetail);

module.exports = {orderDetailRouter}