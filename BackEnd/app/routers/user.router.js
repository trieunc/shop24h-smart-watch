const express = require("express");
const userController = require("../controllers/user.controller");
const userMiddleware = require("../middlewares/user.middleware");
const userRouter = express.Router();

// LOGIN USER
userRouter.post("/login", userController.loginUser);

// REFRESH TOKEN
userRouter.post("/refresh", userController.refreshToken);

// LOGOUT USER
userRouter.post("/logout", userMiddleware.verifyToken, userController.logoutUser);

module.exports = { userRouter };