import { Box, Button, Card, CardContent, Checkbox, FormControlLabel, Grid, IconButton, InputAdornment, Paper, TextField, ThemeProvider, Typography, createTheme } from "@mui/material";
import GoogleIcon from '@mui/icons-material/Google';
import auth from "../../firebase";
import { GoogleAuthProvider, onAuthStateChanged, signInWithPopup } from "firebase/auth";
import { useDispatch } from "react-redux";
import { authUserErrorAction, btnLoginClickAction, loginSuccessAction, userRegisterTrueAction } from "../../actions/user.action";
import { Link, useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import VisibilityIcon from '@mui/icons-material/Visibility';
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff';

const style = {
    minHeight: '470px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
}

const theme = createTheme({
    palette: {
        info: {
            main: "#404040",
        },
    },
});

const LoginComponent = () => {
    const provider = new GoogleAuthProvider();
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [showPassword, setShowPassword] = useState(false);

    const usernameHandler = (event) => {
        setUsername(event.target.value);
    }

    const passwordHandler = (event) => {
        setPassword(event.target.value);
    }

    const showPasswordHandler = () => {
        setShowPassword(!showPassword);
    }

    const btnLoginHandler = async () => {
        const user = {
            username,
            password
        };
        dispatch(btnLoginClickAction(user, navigate));
    }

    // Đăng nhập bằng Google (Popup)
    const googleLoginHandler = () => {
        signInWithPopup(auth, provider)
            .then((result) => {
                dispatch(loginSuccessAction(result.user));
                // Đăng nhập thành công thì về trang chủ
                navigate("/");
            })
            .catch((error) => {
                dispatch(authUserErrorAction(error));
            });
    }

    // Xử lý sự kiện click button Đăng ký
    const btnRegisterHandler = () => {
        dispatch(userRegisterTrueAction());
    }

    // Kiểm tra nếu đã đăng nhập thì quay về trang chủ
    useEffect(() => {
        onAuthStateChanged(auth, (result) => {
            if (result) {
                navigate("/");
            }
        });
    }, [navigate]);

    return (
        <Box sx={style}>
            <Paper elevation={3} sx={{ mt: 2, borderRadius: 2 }}>
                <Card sx={{ p: 2, borderRadius: 2 }}>
                    <CardContent>
                        <Typography letterSpacing={2} textAlign="center" fontWeight={600} variant="h5">ĐĂNG NHẬP</Typography>
                        {/* USERNAME INPUT */}
                        <TextField 
                            sx={{ mt: 3 }} 
                            value={username} 
                            onChange={usernameHandler} 
                            size="small" 
                            label="Tên đăng nhập" 
                            fullWidth 
                            margin="dense"
                        />
                        {/* PASSWORD INPUT */}
                        <TextField 
                            InputProps={{
                                endAdornment: (
                                    <InputAdornment position="end">
                                        <IconButton onClick={showPasswordHandler}>
                                            { showPassword ? <VisibilityOffIcon fontSize="small" /> : <VisibilityIcon fontSize="small" /> }
                                        </IconButton>
                                    </InputAdornment>
                                )
                            }} 
                            value={password} 
                            onChange={passwordHandler} 
                            size="small" 
                            label="Mật khẩu" 
                            type={showPassword ? "text" : "password"} 
                            fullWidth 
                            margin="dense"
                        />

                        <Grid container justifyContent="space-between" alignItems="center">
                            <Grid item>
                                <FormControlLabel control={<Checkbox size="small" />} label="Ghi nhớ" />
                            </Grid>
                            <Grid item>
                                <Link to="/login/forgotpassword" ><Typography>Quên mật khẩu?</Typography></Link>
                            </Grid>
                        </Grid>

                        <ThemeProvider theme={theme}>
                            <Button sx={{ mt: 3 }} variant="contained" color="info" size="large" fullWidth onClick={btnLoginHandler}>Đăng nhập</Button>
                        </ThemeProvider>

                        <Button sx={{ mt: 2 }} variant="contained" color="error" size="large" fullWidth onClick={googleLoginHandler}>
                            <GoogleIcon fontSize="small" sx={{ mr: 2 }} />Đăng nhập bằng Google
                        </Button>

                        <Box sx={{ display: 'flex', alignItems: 'center', mt: 2 }}>
                            <Box sx={{ flexGrow: 1, height: '1px', backgroundColor: 'grey.400' }} />
                            <Typography sx={{ mx: 2 }}><b>Hoặc</b></Typography>
                            <Box sx={{ flexGrow: 1, height: '1px', backgroundColor: 'grey.400' }} />
                        </Box>

                        <Button sx={{ mt: 2 }} variant="contained" color="inherit" size="large" fullWidth onClick={btnRegisterHandler}>Đăng ký tài khoản</Button>

                    </CardContent>
                </Card>
            </Paper>
        </Box>
    )
}

export default LoginComponent;