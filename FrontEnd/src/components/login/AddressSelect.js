import { FormControl, Grid, InputLabel, MenuItem, Select, TextField } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { registerAddressChangeAction, registerCityChangeAction, registerDistrictChangeAction, registerWardChangeAction } from "../../actions/user.action";

const AddressSelect = () => {
    const { registerData, registerAddressList } = useSelector(reduxData => reduxData.userReducer);
    const dispatch = useDispatch();

    const handleCityChange = (event) => {
        const city = event.target.value;
        const districtList = registerAddressList.cityList.filter(element => element.name === city)[0].districts;
        dispatch(registerCityChangeAction(city, districtList));

        // Nếu thay đổi Tỉnh/thành Phố thì xóa giá trị ở 2 ô Quận/Huyện và Phường/Xã
        dispatch(registerDistrictChangeAction(""));
        dispatch(registerWardChangeAction(""));
    };

    const handleDistrictChange = (event) => {
        const district = event.target.value;
        const wardList = registerAddressList.districtList.filter(element => element.name === district)[0].wards;
        dispatch(registerDistrictChangeAction(district, wardList));

        // Nếu thay đổi Quận/Huyện thì xóa giá trị ở ô Phường/Xã
        dispatch(registerWardChangeAction(""));
    };

    const handleWardChange = (event) => {
        const ward = event.target.value;
        dispatch(registerWardChangeAction(ward));
    };

    return (
        <div>
            <Grid container spacing={1}>
                <Grid item xs={6}>
                    <TextField
                        size="small" label="Địa chỉ"
                        value={registerData.address}
                        onChange={(e) => dispatch(registerAddressChangeAction(e.target.value))}
                        fullWidth margin="dense"
                    />
                </Grid>
                <Grid item xs={6}>
                    <FormControl fullWidth size="small" margin="dense">
                        <InputLabel id="demo-simple-select-label">Thành Phố</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={registerData.city}
                            label="Thành phố"
                            onChange={handleCityChange}
                        >
                            {registerAddressList.cityList.map((element, index) => <MenuItem key={index} value={element.name}>{element.name}</MenuItem>)}
                        </Select>
                    </FormControl>
                </Grid>
            </Grid>

            <Grid container spacing={1}>
                <Grid item xs={6}>
                    <FormControl fullWidth size="small" margin="dense">
                        <InputLabel id="demo-simple-select-label">Quận/Huyện</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={registerData.district}
                            label="Quận/Huyện"
                            onChange={handleDistrictChange}
                        >
                            {registerAddressList.districtList ? registerAddressList.districtList.map((element, index) => <MenuItem key={index} value={element.name}>{element.name}</MenuItem>) : null}
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={6}>
                    <FormControl fullWidth size="small" margin="dense">
                        <InputLabel id="demo-simple-select-label">Phường/Xã</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={registerData.ward}
                            label="Phường/Xã"
                            onChange={handleWardChange}
                        >
                            {registerAddressList.wardList ? registerAddressList.wardList.map((element, index) => <MenuItem key={index} value={element.name}>{element.name}</MenuItem>) : null}
                        </Select>
                    </FormControl>
                </Grid>
            </Grid>
        </div>
    )
}

export default AddressSelect;