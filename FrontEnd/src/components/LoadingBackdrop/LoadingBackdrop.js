import { Backdrop, CircularProgress } from "@mui/material";
import { useSelector } from "react-redux";

const LoadingBackdrop = () => {
    const { isLoading } = useSelector(reduxData => reduxData.productReducer);
    const { loading } = useSelector(reduxData => reduxData.userReducer);

    return (
        <>
            <Backdrop sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }} open={isLoading}>
                <CircularProgress color="inherit" />
            </Backdrop>

            <Backdrop sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 999 }} open={loading}>
                <CircularProgress color="inherit" />
            </Backdrop>
        </>
    )
}

export default LoadingBackdrop;