import ScrollToTopButton from "./ScrollToTopButton/ScrollToTopButton";
import { useEffect, useState } from "react";
import { Route, Routes } from "react-router-dom";
import routeList from "../../routes";
import HomePage from "../../pages/HomePage";
import SnackbarAlert from "../SnackbarAlert/SnackbarAlert";
import LoginRequireModal from "../cart/LoginRequireModal";
import LoadingBackdrop from "../LoadingBackdrop/LoadingBackdrop";

const ContentComponent = () => {
    const [toTopButton, showButton] = useState(false);
    useEffect(() => {
        const handleScroll = () => {
            if (window.scrollY > 100) {
                showButton(true);
            }
            else {
                showButton(false);
            }
        };
        window.addEventListener('scroll', handleScroll);
        return () => window.removeEventListener('scroll', handleScroll);
    }, [])

    return (
        <div>
            {
                <Routes>
                    {routeList.map((route, index) => {
                        if (route.path) {
                            return <Route key={index} path={route.path} element={route.element} />
                        }
                        else {
                            return null;
                        }
                    })}
                    <Route path="*" element={<HomePage />} />
                </Routes>
            }
            <SnackbarAlert />
            {toTopButton ? <ScrollToTopButton /> : null}
            <LoginRequireModal />
            <LoadingBackdrop />
        </div>
    )
}

export default ContentComponent;