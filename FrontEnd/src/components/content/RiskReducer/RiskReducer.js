import freeship from "../../../assets/images/icon-shipping-box.png";
import payment from "../../../assets/images/icon-quick-payment.png";
import support from "../../../assets/images/icon-support.png";
import watch from "../../../assets/images/banner.png"
import { Container, Grid } from "@mui/material";

const RiskReducer = () => {
    return (
        <Container>
            <Grid container mt={4} mb={{sm: 2, lg: 4}}>
                <Grid item sm={12} lg={7}>
                    <img src={watch} width="100%" alt="Banner" />
                </Grid>
                <Grid item sm={12} lg={5} my="auto" mx="auto">
                    <Grid item sm={12} className="text-center">
                        <div className="advantage-shape">
                            <img src={freeship} alt="shipping-box" />
                            <div>
                                <h3>Giao Hàng Hỏa Tốc</h3>
                                <p>Miễn phí đổi trả hàng</p>
                            </div>
                        </div>
                    </Grid>
                    <Grid item sm={12} className="text-center">
                        <div className="advantage-shape">
                            <img src={payment} alt="shipping-box" />
                            <div>
                                <h3>Thanh Toán Tiện Lợi</h3>
                                <p>Giao dịch an toàn</p>
                            </div>
                        </div>
                    </Grid>
                    <Grid item sm={12} className="text-center">
                        <div className="advantage-shape">
                            <img src={support} alt="shipping-box" />
                            <div>
                                <h3>Hỗ Trợ 24/7</h3>
                                <p>Nhanh chóng và hiệu quả</p>
                            </div>
                        </div>
                    </Grid>
                </Grid>
            </Grid>
            <hr className="hr"></hr>
        </Container>
    )
}

export default RiskReducer;