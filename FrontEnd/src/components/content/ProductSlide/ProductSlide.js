import Slider from "react-slick";
import slide_1 from "../../../assets/images/slide/1.png";
import slide_2 from "../../../assets/images/slide/2.png";
import slide_3 from "../../../assets/images/slide/3.png";
import { Grid } from "@mui/material";

const ProductSlide = () => {

    const settings = {
        dots: true,
        infinite: true,
        speed: 1000,
        fade: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 6000,
        cssEase: "ease-in-out",
        arrows: false,
    };

    return (

            <Grid container>
                <Grid item xs={12}>
                    <Slider {...settings}>
                        <div>
                            <img src={slide_1} style={{ width: "100vw" }} alt="Slide-1" />
                        </div>
                        <div>
                            <img src={slide_2} style={{ width: "100vw" }} alt="Slide-2" />
                        </div>
                        <div>
                            <img src={slide_3} style={{ width: "100vw" }} alt="Slide-3" />
                        </div>
                    </Slider>
                </Grid>
            </Grid>

    )
}

export default ProductSlide;