import { Button, ThemeProvider, createTheme } from "@mui/material";

const style = {
    position: "fixed", /* Fixed/sticky position */
    bottom: "20px", /* Place the button at the bottom of the page */
    right: "30px", /* Place the button 30px from the right */
    zIndex: "9999",
    fontSize: "14px",
    height: "60px",
    width: "60px",
    borderRadius: "50%",
    opacity: 0.5,
}

const theme = createTheme({
    palette: {
        info: {
            main: "#000000",
        },
    },
});

const ScrollToTopButton = () => {
    return (
        <a href="#top">
            <ThemeProvider theme={theme}>
                <Button sx={style} color="info" variant="contained" className="btn-to-top">
                    <i className="fa-solid fa-up-long fa-beat"></i>
                </Button>
            </ThemeProvider>

        </a>
    )
}

export default ScrollToTopButton;