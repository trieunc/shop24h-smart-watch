import { Box, Button, Grid, IconButton, Pagination, Paper, styled, Table, TableBody, TableCell, tableCellClasses, TableContainer, TableHead, TableRow, Tooltip, Typography } from "@mui/material";
import AddIcon from '@mui/icons-material/Add';
import EditIcon from '@mui/icons-material/Edit';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { btnCreateOrderClickAction, btnDeleteOrderClickAction, getAllOrdersAction, orderDetailOpenAction, orderPaginationChangeAction } from "../../../actions/user.action";
import createAxiosInterceptorJWT from "../../../createAxiosJWT";
import OrderDetailModal from "./OrderDetailModal";
import DeleteOrderModal from "./DeleteOrderModal";
import CreateOrderModal from "./CreateOrderModal";
import OrderFilter from "./OrderFilter";

const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
        backgroundColor: "#505050",
        color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
        fontSize: 15,
    },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
        border: 0,
    },
}));

const getStatusColor = (paramStatus) => {
    if (paramStatus === "Confirmed") {
        return "blue";
    }
    else if (paramStatus === "Canceled") {
        return "red";
    }
    else if (paramStatus === "Delivered") {
        return "green";
    }
    return "maroon";
}

const OrderAdminPage = () => {
    const { userData, allOrders, orderPagination, renderPage } = useSelector(reduxData => reduxData.userReducer);
    const dispatch = useDispatch();
    const axiosJWT = createAxiosInterceptorJWT(dispatch);
    const skipValue = (orderPagination.currentPage - 1) * orderPagination.limit;

    useEffect(() => {
        dispatch(getAllOrdersAction(userData, axiosJWT, orderPagination.limit, orderPagination.currentPage));
        // eslint-disable-next-line
    }, [orderPagination.currentPage, renderPage]);

    // create order button handler
    const adminCreateOrderHandler = () => {
        dispatch(btnCreateOrderClickAction());
    }

    // edit button handler
    const editHandler = (order) => {
        dispatch(orderDetailOpenAction(order, userData, axiosJWT));
    }

    // delete button handler
    const deleteHandler = (order) => {
        dispatch(btnDeleteOrderClickAction(order._id));
    }

    // pagination change handler
    const orderPaginationChangeHandler = (event, value) => {
        dispatch(orderPaginationChangeAction(value));
    }

    return (
        <Box sx={{ minHeight: 845 }}>
            <Typography textAlign="center" variant="h4" fontWeight={600}>QUẢN LÝ ĐƠN HÀNG</Typography>

            <OrderFilter />

            <Grid container justifyContent="end">
                <Button sx={{ mt: 1 }} startIcon={<AddIcon sx={{ mb: "3px" }} />} variant="contained" size="small" onClick={adminCreateOrderHandler}>Đơn hàng mới</Button>
            </Grid>

            <TableContainer sx={{ mt: 2 }} component={Paper} elevation={2}>
                <Table size="small" aria-label="customized table">
                    <TableHead>
                        <StyledTableRow>
                            <StyledTableCell align="center">STT</StyledTableCell>
                            <StyledTableCell>Mã Đơn Hàng</StyledTableCell>
                            <StyledTableCell>Họ Và Tên</StyledTableCell>
                            <StyledTableCell>Email</StyledTableCell>
                            <StyledTableCell>Điện Thoại</StyledTableCell>
                            <StyledTableCell>Ngày Đặt Hàng</StyledTableCell>
                            <StyledTableCell>Ngày Giao Hàng</StyledTableCell>
                            <StyledTableCell align="center">Trạng Thái</StyledTableCell>
                            <StyledTableCell align="center">Actions</StyledTableCell>
                        </StyledTableRow>
                    </TableHead>
                    <TableBody>
                        {
                            allOrders ? allOrders.map((element, index) => {
                                return (
                                    <StyledTableRow key={index}>
                                        <StyledTableCell align="center">{index + skipValue + 1}</StyledTableCell>
                                        <StyledTableCell>{element.orderCode}</StyledTableCell>
                                        <StyledTableCell>{element.fullName}</StyledTableCell>
                                        <StyledTableCell>{element.email}</StyledTableCell>
                                        <StyledTableCell>{element.phone}</StyledTableCell>
                                        <StyledTableCell>{element.orderDate}</StyledTableCell>
                                        <StyledTableCell>{element.shippedDate ? element.shippedDate : "Chưa hoàn thành"}</StyledTableCell>
                                        <StyledTableCell align="center">
                                                <Typography fontSize={15} fontWeight={600} color={() => getStatusColor(element.status)}>
                                                    {element.status}
                                                </Typography>
                                        </StyledTableCell>
                                        <StyledTableCell align="center">
                                            <Tooltip title="Chi Tiết / Chỉnh Sửa">
                                                <IconButton aria-label="edit" onClick={() => editHandler(element)}>
                                                    <EditIcon fontSize="small" color="success" />
                                                </IconButton>
                                            </Tooltip>
                                            <Tooltip title="Xóa">
                                                <IconButton aria-label="delete" onClick={() => deleteHandler(element)}>
                                                    <DeleteForeverIcon fontSize="small"  color="error" />
                                                </IconButton>
                                            </Tooltip>
                                        </StyledTableCell>
                                    </StyledTableRow>
                                )
                            }) : null
                        }
                    </TableBody>
                </Table>
            </TableContainer>
            <Grid container mt={2} justifyContent="center">
                <Pagination count={orderPagination.totalPages} page={orderPagination.currentPage} onChange={orderPaginationChangeHandler} />
            </Grid>
            <CreateOrderModal />
            <OrderDetailModal />
            <DeleteOrderModal />
        </Box>
    )
}

export default OrderAdminPage;