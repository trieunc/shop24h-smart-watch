import { Button, FormControl, Grid, InputLabel, MenuItem, Paper, Select, TextField, Typography } from "@mui/material";
import FilterAltIcon from '@mui/icons-material/FilterAlt';
import { useState } from "react";

const OrderFilter = () => {
    const [status, setStatus] = useState('');

    const handleStatusFilterChange = (event) => {
        setStatus(event.target.value);
    };
    return (
        <Paper sx={{ p: 1, my: 2 }}>
            <Grid container spacing={3} alignItems="center" justifyContent="center">
                <Grid item>
                    <Typography variant="h6">NHẬP THÔNG TIN CẦN TÌM:</Typography>
                </Grid>
                <Grid item xs={2}>
                    <TextField label="Nội Dung" size="small" fullWidth />
                </Grid>
                <Grid item xs={2}>
                    <FormControl size="small" fullWidth>
                        <InputLabel>Trạng Thái</InputLabel>
                        <Select
                            value={status}
                            label="Trạng Thái"
                            onChange={handleStatusFilterChange}
                        >
                            <MenuItem value="New">New</MenuItem>
                            <MenuItem value="Confirmed">Confirmed</MenuItem>
                            <MenuItem value="Delivered">Delivered</MenuItem>
                            <MenuItem value="Canceled">Canceled</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item>
                    <Button color="inherit" variant="outlined" startIcon={<FilterAltIcon />}>Lọc Dữ Liệu</Button>
                </Grid>
            </Grid>
        </Paper>
    )
}

export default OrderFilter;