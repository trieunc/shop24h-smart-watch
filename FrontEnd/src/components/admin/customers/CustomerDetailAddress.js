import { FormControl, Grid, InputLabel, MenuItem, Select, TextField } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { customerDetailAddressChangeAction, customerDetailCityChangeAction, customerDetailDistrictChangeAction, customerDetailWardChangeAction } from "../../../actions/user.action";

const CustomerDetailAddress = () => {
    const { customerDetail, customerDetailAddressList } = useSelector(reduxData => reduxData.userReducer);
    const dispatch = useDispatch();

    const handleCityChange = (event) => {
        const city = event.target.value;
        // Lọc lấy danh sách Quận/Huyện theo Tỉnh/Thành phố đã chọn
        const districtList = customerDetailAddressList.cityList.filter(element => element.name === city)[0].districts;
        dispatch(customerDetailCityChangeAction(city, districtList));

        // Nếu thay đổi Tỉnh/thành Phố thì xóa giá trị ở 2 ô Quận/Huyện và Phường/Xã
        dispatch(customerDetailDistrictChangeAction(""));
        dispatch(customerDetailWardChangeAction(""));
    };

    const handleDistrictChange = (event) => {
        const district = event.target.value;
        // Lọc lấy danh sách Phường/Xã theo Quận/Huyện đã chọn
        const wardList = customerDetailAddressList.districtList.filter(element => element.name === district)[0].wards;
        dispatch(customerDetailDistrictChangeAction(district, wardList));

        // Nếu thay đổi Quận/Huyện thì xóa giá trị ở ô Phường/Xã
        dispatch(customerDetailWardChangeAction(""));
    };

    const handleWardChange = (event) => {
        const ward = event.target.value;
        dispatch(customerDetailWardChangeAction(ward));
    };

    return (
        <Grid container spacing={2}>
            <Grid item xs={6}>
                <TextField
                    value={customerDetail.address}
                    onChange={(e) => dispatch(customerDetailAddressChangeAction(e.target.value))}
                    label="Địa Chỉ" fullWidth size="small" margin="dense"
                />
                <FormControl fullWidth size="small" margin="dense">
                    <InputLabel>Quận/Huyện</InputLabel>
                    <Select
                        value={customerDetail.district} label="Quận/Huyện"
                        onChange={handleDistrictChange}
                    >
                        {customerDetailAddressList.districtList ? customerDetailAddressList.districtList.map((element, index) => <MenuItem key={index} value={element.name}>{element.name}</MenuItem>) : null}
                    </Select>
                </FormControl>
            </Grid>
            <Grid item xs={6}>
                <FormControl fullWidth size="small" margin="dense">
                    <InputLabel>Thành Phố</InputLabel>
                    <Select
                        value={customerDetail.city} label="Thành phố"
                        onChange={handleCityChange}
                    >
                        {customerDetailAddressList.cityList.map((element, index) => <MenuItem key={index} value={element.name}>{element.name}</MenuItem>)}
                    </Select>
                </FormControl>

                <FormControl fullWidth size="small" margin="dense">
                    <InputLabel>Phường/Xã</InputLabel>
                    <Select
                        value={customerDetail.ward} label="Phường/Xã"
                        onChange={handleWardChange}
                    >
                        {customerDetailAddressList.wardList ? customerDetailAddressList.wardList.map((element, index) => <MenuItem key={index} value={element.name}>{element.name}</MenuItem>) : null}
                    </Select>
                </FormControl>
            </Grid>
        </Grid>
    )
}

export default CustomerDetailAddress;