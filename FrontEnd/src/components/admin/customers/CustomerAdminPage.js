import { Box, Button, Grid, IconButton, Pagination, Paper, styled, Table, TableBody, TableCell, tableCellClasses, TableContainer, TableHead, TableRow, Tooltip, Typography } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import EditIcon from '@mui/icons-material/Edit';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import AddIcon from '@mui/icons-material/Add';
import createAxiosInterceptorJWT from "../../../createAxiosJWT";
import { adminCustomerPaginationChangeAction, adminDeleteCustomerModalOpenAction, customerOrderModalOpenAction, getAllCustomersAction, getCustomerDetailAction } from "../../../actions/user.action";
import CustomerDetailModal from "./CustomerDetailModal";
import CustomerFilter from "./CustomerFilter";
import DeleteCustomerModal from "./DeleteCustomerModal";
import CustomerOrderModal from "./CustomerOrderModal";

const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
        backgroundColor: "#505050",
        color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
        fontSize: 15,
    },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
        border: 0,
    },
}));

const CustomerAdminPage = () => {
    const { userData, allCustomers, customerPagination, renderPage } = useSelector(reduxData => reduxData.userReducer);
    const skipValue = (customerPagination.currentPage - 1) * customerPagination.limit;
    const dispatch = useDispatch();
    const axiosJWT = createAxiosInterceptorJWT(dispatch);

    // load all customers on load:
    useEffect(() => {
        dispatch(getAllCustomersAction(userData, axiosJWT, customerPagination.limit, customerPagination.currentPage));
        // eslint-disable-next-line
    }, [customerPagination.currentPage, renderPage]);

    // button edit/detail handler
    const editCustomerHandler = (customer) => {
        const customerId = customer._id;
        dispatch(getCustomerDetailAction(userData, customerId, axiosJWT));
    };

    // button delete handler
    const deleteCustomerHandler = (customer) => {
        const customerId = customer._id;
        dispatch(adminDeleteCustomerModalOpenAction(customerId));
    }

    // button customer total order handler
    const totalOrderHandler = (customer) => {
        const orders = customer.orders;
        dispatch(customerOrderModalOpenAction(orders, userData, axiosJWT));
    }

    // pagination change handler
    const paginationChangeHandler = (event, value) => {
        dispatch(adminCustomerPaginationChangeAction(value));
    }

    return (
        <Box sx={{ minHeight: 845 }}>
            <Typography textAlign="center" variant="h4" fontWeight={600}>QUẢN LÝ KHÁCH HÀNG</Typography>

            <CustomerFilter />

            <Grid container justifyContent="end">
                <Button sx={{ mt: 1 }} startIcon={<AddIcon sx={{ mb: "3px" }} />} variant="contained" size="small">Khách hàng mới</Button>
            </Grid>

            <TableContainer sx={{ mt: 2 }} component={Paper} elevation={2}>
                <Table size="small" aria-label="customized table">
                    <TableHead>
                        <StyledTableRow>
                            <StyledTableCell align="center">STT</StyledTableCell>
                            <StyledTableCell style={{ whiteSpace: "nowrap" }}>Họ Và Tên Khách Hàng</StyledTableCell>
                            <StyledTableCell>Username</StyledTableCell>
                            <StyledTableCell>Hạng</StyledTableCell>
                            <StyledTableCell>Email</StyledTableCell>
                            <StyledTableCell>Điện Thoại</StyledTableCell>
                            <StyledTableCell>Địa Chỉ</StyledTableCell>
                            <StyledTableCell align="center">Đơn Hàng</StyledTableCell>
                            <StyledTableCell align="center" style={{ whiteSpace: "nowrap" }}>Actions</StyledTableCell>
                        </StyledTableRow>
                    </TableHead>
                    <TableBody>
                        {
                            allCustomers ? allCustomers.map((element, index) => {
                                return (
                                    <StyledTableRow key={index}>
                                        <StyledTableCell align="center">{index + skipValue + 1}</StyledTableCell>
                                        <StyledTableCell style={{ whiteSpace: "nowrap" }}>
                                            <img src={element.profilePic} style={{ width: "35px", borderRadius: "50%", marginRight: "10px" }} alt="profile avatar" />
                                            {element.fullName}
                                        </StyledTableCell>
                                        <StyledTableCell>{element.user.username}</StyledTableCell>
                                        <StyledTableCell>{element.user.role}</StyledTableCell>
                                        <StyledTableCell>{element.email}</StyledTableCell>
                                        <StyledTableCell>{element.phone}</StyledTableCell>
                                        <StyledTableCell>{`${element.address}, ${element.ward}, ${element.district}, ${element.city}`}</StyledTableCell>
                                        <StyledTableCell align="center">
                                            <Button color="inherit" onClick={() => totalOrderHandler(element)}>
                                                {element.orders.length}
                                            </Button>
                                        </StyledTableCell>
                                        <StyledTableCell align="center" style={{ whiteSpace: "nowrap" }}>
                                            <Tooltip title="Chi Tiết / Chỉnh Sửa">
                                                <IconButton aria-label="edit" onClick={() => editCustomerHandler(element)}>
                                                    <EditIcon fontSize="small" color="success" />
                                                </IconButton>
                                            </Tooltip>
                                            <Tooltip title="Xóa">
                                                <IconButton aria-label="delete" onClick={() => deleteCustomerHandler(element)}>
                                                    <DeleteForeverIcon fontSize="small" color="error" />
                                                </IconButton>
                                            </Tooltip>
                                        </StyledTableCell>
                                    </StyledTableRow>
                                )
                            }) : null
                        }
                    </TableBody>
                </Table>
            </TableContainer>

            <Grid container mt={2} justifyContent="center">
                <Pagination count={customerPagination.totalPages} page={customerPagination.currentPage} onChange={paginationChangeHandler} />
            </Grid>

            <CustomerDetailModal />
            <DeleteCustomerModal />
            <CustomerOrderModal />
        </Box>
    )
}

export default CustomerAdminPage;