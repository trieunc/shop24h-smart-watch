import { Card, CardContent, CardMedia, Grid, Typography } from "@mui/material";
import { useNavigate } from "react-router-dom";

const ProductCard4 = (props) => {
    const { data } = props;
    const navigate = useNavigate();

    const cardClickHandler = () => {
        navigate(`/products/${data._id}`);
        window.scrollTo(0, 0);
    }

    return (
        <Grid item xs={6} sm={4} md={3}>
            <Card className="product card" onClick={cardClickHandler}>
                <CardMedia sx={{ p: 1, width: "100%" }} component="img" image={data.imageUrl} title="smart-watch" />
                <CardContent>
                    <Typography sx={{ fontWeight: 500 }} tag="h6">{data.name}</Typography>
                    <Typography sx={{ mt: 1 }} tag="h6"> <small><del>{data.buyPrice.toLocaleString('en-US')}</del></small> <b>{data.promotionPrice.toLocaleString('en-US')}đ</b></Typography>
                    <Typography sx={{ mt: 1 }}>
                        <small>{data.description}</small>
                    </Typography>
                </CardContent>
            </Card>
        </Grid>
    )
}

export default ProductCard4;