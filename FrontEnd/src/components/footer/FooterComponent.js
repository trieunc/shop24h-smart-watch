import { Container, Grid } from "@mui/material";
import Information from "./Information/Information";
import Menu from "./Menu/Menu";
import Help from "./Help/Help";
import Question from "./Question/Question";
import Copyright from "./Copyright/Copyright";

const FooterComponent = () => {
    return (
        <Container maxWidth="" sx={{ p: 2, backgroundColor: "#f0f0f0" }}>
            <Container>
                <Grid container>
                    <Information />
                    <Menu />
                    <Help />
                    <Question />
                </Grid>
                <Grid container mt={2}>
                    <Copyright />
                </Grid>
            </Container>
        </Container>
    )
}

export default FooterComponent;