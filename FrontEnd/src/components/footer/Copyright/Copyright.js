import { Grid } from "@mui/material";

const Copyright = () => {
    return (
        <Grid item xs={12} textAlign="center">
            <p><small>Copyright © 2023 <span><a href="#top"><b>2T SMART SHOP</b></a></span> All rights reserved</small></p>
        </Grid>
    )
}

export default Copyright;