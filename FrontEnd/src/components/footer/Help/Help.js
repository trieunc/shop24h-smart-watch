import { Grid } from "@mui/material";

const Help = () => {
    return (
        <Grid className="slide-link" item xs={12} md={3}>
            <div className="footer-text">
                <h2 className="footer-header">Trợ Giúp</h2>
                <p><a href="#shop">Vận Chuyển</a></p>
                <p><a href="#shop">Hoàn & Trả Hàng</a></p>
                <p><a href="#shop">Điều Khoản Dịch Vụ</a></p>
                <p><a href="#shop">Chính Sách Bảo Mật</a></p>
            </div>
        </Grid>
    )
}

export default Help;