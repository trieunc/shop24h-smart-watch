import TopPageInfo from "./Infornation/TopPageInfo";
import NavbarFixedTop from "./NavBar/NavbarFixedTop";

const HeaderComponent = () => {
    return (
        <div>
            <TopPageInfo />
            <NavbarFixedTop />
        </div>
    )
}

export default HeaderComponent;