import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { AppBar, Avatar, Box, Button, CardMedia, Container, IconButton, Menu, MenuItem, Toolbar, Tooltip, Typography } from "@mui/material";
import { Link, useNavigate } from "react-router-dom";
import { signOut } from "firebase/auth";
import { logoutSuccessAction } from "../../../actions/user.action";
import AccountBoxIcon from '@mui/icons-material/AccountBox';
import PersonIcon from '@mui/icons-material/Person';
import LogoutIcon from '@mui/icons-material/Logout';
import MenuIcon from '@mui/icons-material/Menu';
import logo from "../../../assets/images/logo2T.png"
import routeList from "../../../routes";
import auth from "../../../firebase";
import createAxiosInterceptorJWT from "../../../createAxiosJWT";

const NavbarFixedTop = () => {
    // ============ USER AVATAR DISPLAY HANDLE ============
    const { userData } = useSelector(reduxData => reduxData.userReducer);
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const axiosJWT = createAxiosInterceptorJWT(dispatch);

    const logoutHandle = () => {
        handleCloseUserMenu();
        signOut(auth)
            .then(() => {
                // Sign-out successful.
                dispatch(logoutSuccessAction(axiosJWT));
                navigate("/");
            })
    }

    const dashboardMenuClickHandler = () => {
        handleCloseUserMenu();
        navigate(userData.role === "admin" ? "/admin" : "/user")
    }

    useEffect(() => {
        // Sticky Menu Area
        window.addEventListener('scroll', isSticky);
        return () => {
            window.removeEventListener('scroll', isSticky);
        };
    }, [userData]);

    /* Method that will fix header after a specific scrollable */
    const isSticky = () => {
        const header = document.querySelector('.header-section');
        const scrollTop = window.scrollY;
        scrollTop >= 120 ? header.classList.add('is-sticky') : header.classList.remove('is-sticky');
    };

    // Set Appbar Anchor Element
    const [anchorElNav, setAnchorElNav] = useState(null);
    const [anchorElUser, setAnchorElUser] = useState(null);
    const handleOpenNavMenu = (event) => {
        setAnchorElNav(event.currentTarget);
    };
    const handleOpenUserMenu = (event) => {
        setAnchorElUser(event.currentTarget);
    };
    const handleCloseNavMenu = () => {
        setAnchorElNav(null);
    };
    const handleCloseUserMenu = () => {
        setAnchorElUser(null);
    };

    return (
        <>
            <AppBar position="static" elevation={2} className="header-section" sx={{ backgroundColor: "#f0f0f0" }}>
                <Container>
                    <Toolbar disableGutters>
                        <Link to="/"><CardMedia component="img" image={logo} sx={{ width: 40, mr: 2, display: { xs: 'none', md: 'flex' } }} /></Link>
                        <Typography
                            variant="h6"
                            noWrap
                            component="a"
                            href="/"
                            sx={{
                                mr: 2,
                                my: 2,
                                display: { xs: 'none', md: 'flex' },
                                fontWeight: 700,
                                letterSpacing: '4px',
                                color: 'black',
                                textDecoration: 'none',
                            }}
                        >
                            2T SMART SHOP
                        </Typography>

                        <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
                            <IconButton
                                size="large"
                                aria-label="account of current user"
                                aria-controls="menu-appbar"
                                aria-haspopup="true"
                                onClick={handleOpenNavMenu}
                                color="black"
                            >
                                <MenuIcon />
                            </IconButton>
                            <Menu
                                id="menu-appbar"
                                anchorEl={anchorElNav}
                                anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
                                keepMounted
                                transformOrigin={{ vertical: 'top', horizontal: 'left' }}
                                open={Boolean(anchorElNav)}
                                onClose={handleCloseNavMenu}
                                sx={{ display: { xs: 'block', md: 'none' } }}
                            >
                                {routeList.map((page, index) => (
                                    page.label ?
                                        <MenuItem className="zoom-link" key={index} onClick={handleCloseNavMenu}>
                                            <Typography sx={{ px: 4 }} textAlign="center"><Link to={page.path}>{page.label}</Link></Typography>
                                        </MenuItem>
                                        : null
                                ))}
                                {
                                    userData ? null
                                        :
                                        <MenuItem onClick={handleCloseNavMenu}>
                                            <Link to="/login">
                                                <IconButton
                                                    onClick={handleCloseNavMenu}
                                                    sx={{ ml: 3, color: 'black', display: 'block', fontSize: "12px", letterSpacing: "2px" }}
                                                >
                                                    <Tooltip title="Đăng Nhập">
                                                        <PersonIcon />
                                                    </Tooltip>
                                                </IconButton>
                                            </Link>
                                        </MenuItem>
                                }
                            </Menu>
                        </Box>
                        <Typography
                            variant="h5"
                            noWrap
                            component="a"
                            href="/"
                            sx={{
                                my: 2,
                                display: { xs: 'flex', md: 'none' },
                                flexGrow: 1,
                                fontWeight: 700,
                                letterSpacing: '3px',
                                color: 'black',
                                textDecoration: 'none',
                            }}
                        >
                            2T SMART SHOP
                        </Typography>
                        <Box className="zoom-link" sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' }, justifyContent: "flex-end", alignItems: "center" }}>
                            {routeList.map((page, index) => (
                                page.label ?
                                    <Link to={page.path} key={index}><Button
                                        key={index}
                                        onClick={handleCloseNavMenu}
                                        sx={{ mx: 3, color: 'black', display: 'block', fontSize: "12px", letterSpacing: "2px" }}
                                    >
                                        {page.label}
                                    </Button></Link>
                                    :
                                    null
                            ))}
                            {
                                userData ? null
                                    :
                                        <IconButton
                                            href="/login"
                                            onClick={handleCloseNavMenu}
                                            sx={{ ml: 3, color: 'black', display: 'block', fontSize: "12px", letterSpacing: "2px" }}
                                        >
                                            <Tooltip title="Đăng Nhập">
                                                <PersonIcon />
                                            </Tooltip>
                                        </IconButton>
                            }
                        </Box>
                        {
                            (userData)
                                ?
                                <Box sx={{ flexGrow: 0, ml: 3 }}>
                                    <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                                        <Avatar alt="Remy Sharp" src={userData.photoURL || userData.profilePic} />
                                    </IconButton>
                                    <Menu
                                        sx={{ mt: '45px' }}
                                        id="menu-appbar"
                                        anchorEl={anchorElUser}
                                        anchorOrigin={{
                                            vertical: 'top',
                                            horizontal: 'right',
                                        }}
                                        keepMounted
                                        transformOrigin={{
                                            vertical: 'top',
                                            horizontal: 'right',
                                        }}
                                        open={Boolean(anchorElUser)}
                                        onClose={handleCloseUserMenu}
                                    >
                                        <MenuItem onClick={handleCloseUserMenu}>
                                            <Typography textAlign="center">Xin Chào: <b>{userData.displayName || userData.fullName} </b></Typography>
                                        </MenuItem>
                                        <MenuItem onClick={handleCloseUserMenu}>
                                            <Typography textAlign="center">Hạng: <b style={{textTransform: "capitalize"}}>{userData.role || "User"} </b></Typography>
                                        </MenuItem>
                                        <MenuItem onClick={dashboardMenuClickHandler}>
                                            <Typography textAlign="center">
                                                <AccountBoxIcon sx={{ mr: 1 }} />{userData.role === "admin" ? "Quản Trị" : "Hồ Sơ"}
                                            </Typography>
                                        </MenuItem>
                                        <MenuItem onClick={logoutHandle}>
                                            <Typography textAlign="center"><LogoutIcon sx={{ mr: 1 }} /> Đăng Xuất</Typography>
                                        </MenuItem>
                                    </Menu>
                                </Box>
                                :
                                null
                        }
                    </Toolbar>
                </Container>
            </AppBar>
        </>
    )
}

export default NavbarFixedTop;