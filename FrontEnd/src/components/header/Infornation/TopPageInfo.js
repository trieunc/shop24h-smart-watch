import { Container, Grid } from "@mui/material";
import PhoneIcon from '@mui/icons-material/Phone';
import EmailIcon from '@mui/icons-material/Email';
import LocalShippingIcon from '@mui/icons-material/LocalShipping';


const TopPageInfo = () => {
    return (
        <Container maxWidth="" sx={{py: 1, backgroundColor: "black"}}>
                <Container>
                    <Grid container>
                        <Grid item lg={12}>
                            <Grid container>
                                <Grid item lg={4} sm={12} className="topper">
                                    <span><PhoneIcon fontSize="small" /> &nbsp; +84 916 949 139</span>
                                </Grid>
                                <Grid item lg={3} sm={12} className="topper">
                                    <span><EmailIcon fontSize="small" /> &nbsp; trieu.nc1993@gmail.com</span>
                                </Grid>
                                <Grid item lg={5} sm={12} textAlign={{lg: "right"}} className="topper">
                                    <span><LocalShippingIcon fontSize="small" /> &nbsp; Giao hàng hỏa tốc &amp; Miễn phí đổi trả hàng</span>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Container>
        </Container>
    )
}

export default TopPageInfo;