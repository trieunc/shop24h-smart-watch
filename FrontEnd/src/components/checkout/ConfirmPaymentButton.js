import { Button, Grid, ThemeProvider, createTheme } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { checkInfoAndCreateOrderAction } from "../../actions/user.action";
import createAxiosInterceptorJWT from "../../createAxiosJWT";


const theme = createTheme({
    palette: {
        info: {
            main: "#404040",
        },
    },
});

const ConfirmPaymentButton = () => {
    const { userData } = useSelector(reduxData => reduxData.userReducer);
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const axiosJWT = createAxiosInterceptorJWT(dispatch);
    const cartProducts = JSON.parse(localStorage.getItem("cart"));

    // handle confirm payment click
    const confirmPaymentHandler = () => {
        dispatch(checkInfoAndCreateOrderAction(userData, cartProducts, axiosJWT));
    }


    return (
        <Grid container my={1} spacing={1} justifyContent="center">
            <Grid item>
                <Button color="inherit" variant="contained" onClick={() => navigate("/cart")}>Quay lại giỏ hàng</Button>
            </Grid>
            <Grid item>
                <ThemeProvider theme={theme}>
                    <Button color="info" variant="contained" onClick={confirmPaymentHandler}>Xác Nhận Thanh Toán</Button>
                </ThemeProvider>
            </Grid>
        </Grid>
    )
}

export default ConfirmPaymentButton;