import { Button, Card, CardMedia, createTheme, Grid, Paper, ThemeProvider, Typography } from "@mui/material";
import { useSelector } from "react-redux";
import cartSuccess from "../../assets/images/cart_success.png";

const theme = createTheme({
    palette: {
        info: {
            main: "#404040",
        },
    },
});

const OrderSuccess = () => {
    const { orderCode } = useSelector(reduxData => reduxData.userReducer);

    return (
        <Grid container p={2} alignItems="center" justifyContent="center">
            <Grid xs={12} md={9} item>
                <Paper elevation={2} sx={{ borderRadius: 2 }}>
                    <Card sx={{ p: 2, borderRadius: 2 }}>
                        <Grid container justifyContent="center">
                            <CardMedia component="img" sx={{ width: 150 }} image={cartSuccess} />
                        </Grid>
                        <Typography textAlign="center" variant="h4" fontWeight={600}>ĐẶT HÀNG THÀNH CÔNG!</Typography>
                        <Typography mt={2} textAlign="center" variant="h5" fontWeight={600}>Mã đơn hàng: <u>{orderCode}</u></Typography>
                        <Typography mt={2} textAlign="center" fontSize={17}>Cảm ơn Quý khách đã tin tưởng đặt hàng tại <b>2T Smart Shop</b>.</Typography>
                        <Typography textAlign="center" fontSize={17}>Bộ phận Kinh Doanh sẽ liên hệ Quý khách trong thời gian sớm nhất có thể.</Typography>
                        <Typography textAlign="center" fontSize={17}>Trong trường hợp cần hỗ trợ gấp, Quý khách vui lòng liên hệ số hotline <b>1900130593</b></Typography>
                        <Grid container mt={1} justifyContent="center" spacing={1}>
                            <Grid item>
                                <Button variant="contained" color="inherit">Đơn hàng</Button>
                            </Grid>
                            <Grid item>
                                <ThemeProvider theme={theme}>
                                    <Button variant="contained" color="info">Về trang chủ</Button>
                                </ThemeProvider>
                            </Grid>
                        </Grid>
                    </Card>
                </Paper>
            </Grid>
        </Grid>
    )
}

export default OrderSuccess;