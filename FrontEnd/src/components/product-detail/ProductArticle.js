import { Button, Collapse, Grid, Paper } from "@mui/material"
import parse from 'html-react-parser';
import { useState } from "react";

const ProductArticle = (props) => {
    const { productDetail } = props;
    const [isExpanded, setIsExpanded] = useState(false);

    // function handle click button expand
    const handleExpandClick = () => {
        setIsExpanded(!isExpanded);
    }

    const parsedContent = parse(productDetail.article);

    return (
        <Grid container justifyContent="center">
            <Grid item xs={12} md={10} textAlign="center" className="product-article">
                <Paper sx={{ mt: 2, py: 2, px: 4 }} elevation={1}>
                    <Collapse in={isExpanded} collapsedSize={300}>
                        {parsedContent}
                    </Collapse>
                    <Button sx={{ mt: 3 }} color="inherit" variant="outlined" onClick={handleExpandClick}>{isExpanded ? "Thu gọn" : "Mở rộng"}</Button>
                </Paper>
            </Grid>
        </Grid>
    )
}

export default ProductArticle;