import { Button, Container, Grid, ThemeProvider, createTheme } from "@mui/material";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { createNewCheckoutAction, openLoginRequireModalAction } from "../../actions/user.action";

const theme = createTheme({
    palette: {
        info: {
            main: "#404040",
        },
    },
});

const PaymentButton = () => {
    const { userData } = useSelector(reduxData => reduxData.userReducer);
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const paymentButtonHandler = () => {
        if (!userData) {
            dispatch(openLoginRequireModalAction());
        }
        else {
            // set value state of orderCode="" and orderValue=false
            dispatch(createNewCheckoutAction());
            navigate("/checkout");
        }
    }

    return (
        <>
            <Container>
                <Grid container mt={1} justifyContent="center" spacing={1}>
                    <Grid item>
                        <Button variant="contained" color="inherit" onClick={() => navigate("/products")}>Tiếp tục mua sắm</Button>
                    </Grid>
                    <Grid item>
                        <ThemeProvider theme={theme}>
                            <Button variant="contained" color="info" onClick={paymentButtonHandler}>Đến Trang Thanh Toán</Button>
                        </ThemeProvider>
                    </Grid>
                </Grid>
            </Container>
        </>
    )
}

export default PaymentButton;