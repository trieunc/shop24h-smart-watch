import LocalMallIcon from '@mui/icons-material/LocalMall';
import { Badge } from '@mui/material';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { productCartUpdateAction } from '../../actions/product.action';

const CartLabel = () => {
    const { cartNumber } = useSelector(reduxData => reduxData.productReducer);
    const dispatch = useDispatch();

    useEffect(() => {
        // lấy dữ liệu từ local storage, nếu chưa có dữ liệu sẽ trả về mảng rỗng
        const localData = JSON.parse(localStorage.getItem("cart")) || [];
        // dispatch data cart number về reducer
        dispatch(productCartUpdateAction(localData.length));
    }, [dispatch]);

    return (
        <Badge badgeContent={cartNumber} color="error">
            <LocalMallIcon />
        </Badge>
    )
}

export default CartLabel;