import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "bootstrap/dist/css/bootstrap.min.css";
import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';
import './App.css';

import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { loginSuccessAction } from "./actions/user.action";
import { onAuthStateChanged } from "firebase/auth";
import auth from "./firebase";
import { Route, Routes } from "react-router-dom";
import SnackbarAlert from "./components/SnackbarAlert/SnackbarAlert";
import ScrollToTopButton from "./components/content/ScrollToTopButton/ScrollToTopButton";
import LoginRequireModal from "./components/cart/LoginRequireModal";
import LoadingBackdrop from "./components/LoadingBackdrop/LoadingBackdrop";
import routeList from "./routes";
import HomePage from "./pages/HomePage";


function App() {
  const dispatch = useDispatch(); const [toTopButton, showButton] = useState(false);

  useEffect(() => {
    onAuthStateChanged(auth, (result) => {
      if (result) {
        dispatch(loginSuccessAction(result));
      }
    });
    const handleScroll = () => {
      if (window.scrollY > 100) {
        showButton(true);
      }
      else {
        showButton(false);
      }
    };
    window.addEventListener('scroll', handleScroll);
    return () => window.removeEventListener('scroll', handleScroll);
  }, [dispatch])

  return (
    <div>
      {
        <Routes>
          {routeList.map((route, index) => {
            if (route.path) {
              return <Route key={index} path={route.path} element={route.element} />
            }
            else {
              return null;
            }
          })}
          <Route path="*" element={<HomePage />} />
        </Routes>
      }
      <SnackbarAlert />
      {toTopButton ? <ScrollToTopButton /> : null}
      <LoginRequireModal />
      <LoadingBackdrop />
    </div>
  );
}

export default App;