import CartLabel from "./components/cart/CartLabel";
import AboutUsPage from "./pages/AboutUsPage";
import AdminPage from "./pages/AdminPage";
import CartPage from "./pages/CartPage";
import CheckoutPage from "./pages/CheckoutPage";
import HomePage from "./pages/HomePage";
import LoginPage from "./pages/LoginPage";
import ProductDetailPage from "./pages/ProductDetailPage";
import ProductPage from "./pages/ProductPage";

const routeList = [
    {label: "Trang Chủ", path: "/", element: <HomePage />},
    {label: "Sản Phẩm", path: "/products", element: <ProductPage />},
    {label: "Giới Thiệu", path: "/about", element: <AboutUsPage />},
    {path: "/login", element: <LoginPage />},
    {path: "/products/:productId", element: <ProductDetailPage />},
    {label: <CartLabel />, path: "/cart", element: <CartPage />},
    {path: "/checkout", element: <CheckoutPage />},
    {path: "/admin", element: <AdminPage />},
]

export default routeList;