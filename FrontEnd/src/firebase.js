// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyAuHfpsbiygY_9uB621yNbfDvHpcOkbK_I",
  authDomain: "shop24h-trieungo.firebaseapp.com",
  projectId: "shop24h-trieungo",
  storageBucket: "shop24h-trieungo.appspot.com",
  messagingSenderId: "822054873722",
  appId: "1:822054873722:web:cdc5b581ceae02573ca9c3"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

// Initialize Firebase Authentication and get a reference to the service
const auth = getAuth(app);

export default auth;