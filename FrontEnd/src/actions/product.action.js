import { 
    ADD_TO_CART_SUCCESS,
    BUTTON_FILTER_CLICK, 
    CLOSE_SNACKBAR_ERROR, 
    CLOSE_SNACKBAR_SUCCESS, 
    FILTER_BRAND_CHECKED, 
    FILTER_BRAND_UNCHECKED, 
    FILTER_MAX_PRICE_CHANGE, 
    FILTER_MIN_PRICE_CHANGE, 
    FILTER_NAME_CHANGE, 
    GET_ALL_PRODUCTS, 
    GET_PRODUCT_DATA_ERROR, 
    GET_PRODUCT_DATA_SUCCESS, 
    LOADING_DATA_FINISH, 
    LOADING_DATA_PENDING, 
    PAGINATION_CHANGE, 
    PRODUCT_CART_UPDATE, 
    REMOVE_FROM_CART_SUCCESS
} from "../constants/product.const"

export const getProductDataAction = (currentPage, limit, filterName, filterMinPrice, filterMaxPrice, filterBrand) => async (dispatch) => {
    try {
        await dispatch({
            type: LOADING_DATA_PENDING
        })
        // tạo pagination param gồm giá trị limit và skip:
        const paginationParams = new URLSearchParams({
            skip: (currentPage - 1) * limit,
            limit: limit
        });

        // tạo filter product params:
        const filterParams = new URLSearchParams();
        const filterData = {
            name: filterName.toLowerCase(),
            min: filterMinPrice,
            max: filterMaxPrice,
            brand: filterBrand
        };

        for (const [key, value] of Object.entries(filterData)) {
            if (Array.isArray(value)) {
                for (const item of value) {
                    filterParams.append(key, item);
                }
            } else {
                filterParams.append(key, value);
            }
        }
        
        // fetch API lấy thông tin sản phẩm. Nếu ko có filter data sẽ trả về toàn bộ sản phẩm
        const allFilterProductRes = await fetch(`http://localhost:8000/products?${filterParams}`);
        const allFilterProductData = await allFilterProductRes.json();

        // tính giá trị totalPage
        const totalProducts = allFilterProductData.data.length;
        const totalPage = Math.ceil(totalProducts / limit);

        // fetch API lấy thông tin sản phẩm theo pagination
        const response = await fetch(`http://localhost:8000/products?${filterParams}&${paginationParams}`);
        const productData = await response.json();

        return dispatch({
            type: GET_PRODUCT_DATA_SUCCESS,
            payload: {
                totalPage,
                data: productData.data,
                allProducts: allFilterProductData.data
            }
        })
    }

    catch (err) {
        return dispatch({
            type: GET_PRODUCT_DATA_ERROR,
            payload: err
        })
    }
}

export const getAllProductsAction = () => async dispatch => {
    try {
        const response = await fetch("http://localhost:8000/products");
        const data = await response.json();
        return dispatch({
            type: GET_ALL_PRODUCTS,
            payload: data.data
        })
    }

    catch (err) {
        return dispatch({
            type: GET_PRODUCT_DATA_ERROR,
            payload: err
        })
    }
}

export const nameFilterChangeAction = (value) => {
    return {
        type: FILTER_NAME_CHANGE,
        payload: value
    }
}

export const minPriceFilterChangeAction = (value) => {
    return {
        type: FILTER_MIN_PRICE_CHANGE,
        payload: value
    }
}

export const maxPriceFilterChangeAction = (value) => {
    return {
        type: FILTER_MAX_PRICE_CHANGE,
        payload: value
    }
}

export const brandFilterChangeAction = (value, isChecked) => {
    if (isChecked) {
        return {
            type: FILTER_BRAND_CHECKED,
            payload: value
        }
    }
    else {
        return {
            type: FILTER_BRAND_UNCHECKED,
            payload: value
        }
    }
}

export const paginationChangeAciton = (currentPage) => {
    return {
        type: PAGINATION_CHANGE,
        payload: currentPage
    }
}

export const btnFilterClickAction = () => {
    return {
        type: BUTTON_FILTER_CLICK
    }
}

export const productCartUpdateAction = (cartNumber) => {
    return {
        type: PRODUCT_CART_UPDATE,
        payload: cartNumber
    }
}

export const addToCartSuccessAction = () => {
    return {
        type: ADD_TO_CART_SUCCESS
    }
}

export const removeFromCartSuccess = () => {
    return {
        type: REMOVE_FROM_CART_SUCCESS
    }
}

export const closeSnackbarSuccessAction = () => {
    return {
        type: CLOSE_SNACKBAR_SUCCESS
    }
}

export const closeSnackbarErrorAction = () => {
    return {
        type: CLOSE_SNACKBAR_ERROR
    }
}

export const loadingDataPendingAction = () => {
    return {
        type: LOADING_DATA_PENDING
    }
}

export const loadingDataFinishAction = () => {
    return {
        type: LOADING_DATA_FINISH
    }
}