import axios from "axios";
import { PRODUCT_CART_UPDATE } from "../constants/product.const";
import {
    ADDRESS_INPUT_CHANGE,
    ADD_PRODUCT_TO_ADMIN_CART,
    ADD_PRODUCT_TO_ADMIN_CART_EMPTY_NAME,
    ADMIN_CHECK_ORDER_INFORMATION,
    ADMIN_CREATE_ORDER_ADDRESS_CHANGE,
    ADMIN_CREATE_ORDER_CITY_CHANGE,
    ADMIN_CREATE_ORDER_DISTRICT_CHANGE,
    ADMIN_CREATE_ORDER_EMAIL_CHANGE,
    ADMIN_CREATE_ORDER_ERROR,
    ADMIN_CREATE_ORDER_MESSAGE_CHANGE,
    ADMIN_CREATE_ORDER_NAME_CHANGE,
    ADMIN_CREATE_ORDER_PHONE_CHANGE,
    ADMIN_CREATE_ORDER_RESET_FORM,
    ADMIN_CREATE_ORDER_START,
    ADMIN_CREATE_ORDER_SUCCESS,
    ADMIN_CREATE_ORDER_USERNAME_CHANGE,
    ADMIN_CREATE_ORDER_WARD_CHANGE,
    ADMIN_GET_ORDER_DETAIL_OF_CUSTOMER_ERROR,
    ADMIN_GET_ORDER_DETAIL_OF_CUSTOMER_START,
    ADMIN_GET_ORDER_DETAIL_OF_CUSTOMER_SUCCESS,
    ADMIN_UPDATE_CUSTOMER_ADDRESS_CHANGE,
    ADMIN_UPDATE_CUSTOMER_CITY_CHANGE,
    ADMIN_UPDATE_CUSTOMER_DISTRICT_CHANGE,
    ADMIN_UPDATE_CUSTOMER_ERROR,
    ADMIN_UPDATE_CUSTOMER_FULLNAME_CHANGE,
    ADMIN_UPDATE_CUSTOMER_PHONE_CHANGE,
    ADMIN_UPDATE_CUSTOMER_START,
    ADMIN_UPDATE_CUSTOMER_SUCCESS,
    ADMIN_UPDATE_CUSTOMER_WARD_CHANGE,
    CHECK_CUSTOMER_UPDATE,
    CHECK_ORDER_INFORMATION,
    CHECK_REGISTER_DATA,
    CITY_INPUT_CHANGE,
    CLOSE_ERROR_ALERT,
    CLOSE_LOGIN_REQUIRE_MODAL,
    CLOSE_SUCCESS_ALERT,
    CREATE_NEW_CHECKOUT,
    CREATE_ORDER_API_ERROR,
    CREATE_ORDER_API_START,
    CREATE_ORDER_API_SUCCESS,
    CREATE_ORDER_CLIENT_ERROR,
    CREATE_ORDER_MODAL_CLOSE,
    CREATE_ORDER_MODAL_OPEN,
    CREATE_USER_API_START,
    CREATE_USER_API_SUCCESS,
    CREATE_USER_SERVER_ERROR,
    CUSTOMER_ADMIN_PAGINATION_CHANGE,
    CUSTOMER_DELETE_MODAL_CLOSE,
    CUSTOMER_DELETE_MODAL_OPEN,
    CUSTOMER_DETAIL_MODAL_CLOSE,
    CUSTOMER_DETAIL_MODAL_OPEN,
    CUSTOMER_ORDER_MODAL_CLOSE,
    CUSTOMER_ORDER_MODAL_OPEN,
    DECREASE_PRODUCT_QUANTITY,
    DELETE_ORDER_MODAL_CLOSE,
    DELETE_ORDER_MODAL_OPEN,
    DISTRICT_INPUT_CHANGE,
    EMAIL_INPUT_CHANGE,
    GET_ALL_CUSTOMERS_ERROR,
    GET_ALL_CUSTOMERS_START,
    GET_ALL_CUSTOMERS_SUCCESS,
    GET_ALL_ORDERS_ERROR,
    GET_ALL_ORDERS_START,
    GET_ALL_ORDERS_SUCCESS,
    GET_CUSTOMER_DETAIL_ERROR,
    GET_CUSTOMER_DETAIL_START,
    GET_CUSTOMER_DETAIL_SUCCESS,
    GET_ORDER_DETAIL_ERROR,
    GET_ORDER_DETAIL_START,
    GET_ORDER_DETAIL_SUCCESS,
    GET_USER_ADDRESS,
    GET_USER_INFORMATION,
    GET_USER_INFOR_BY_EMAIL_ERROR,
    GET_USER_INFOR_BY_EMAIL_START,
    GET_USER_INFOR_BY_EMAIL_SUCCESS,
    GET_USER_INFOR_BY_PHONE_ERROR,
    GET_USER_INFOR_BY_PHONE_START,
    GET_USER_INFOR_BY_PHONE_SUCCESS,
    GET_USER_INFOR_BY_USERNAME_ERROR,
    GET_USER_INFOR_BY_USERNAME_START,
    GET_USER_INFOR_BY_USERNAME_SUCCESS,
    GET_USER_INFOR_EMAIL_EMPTY,
    GET_USER_INFOR_PHONE_EMPTY,
    GET_USER_INFOR_USERNAME_EMPTY,
    INCREASE_PRODUCT_QUANTITY,
    LOGIN_ERROR,
    MESSAGE_INPUT_CHANGE,
    NAME_INPUT_CHANGE,
    OPEN_LOGIN_REQUIRE_MODAL,
    ORDER_ADMIN_PAGINATION_CHANGE,
    ORDER_DELETE_ERROR,
    ORDER_DELETE_START,
    ORDER_DELETE_SUCCESS,
    ORDER_DETAIL_MODAL_CLOSE,
    ORDER_DETAIL_MODAL_OPEN,
    ORDER_NOTE_CHANGE,
    ORDER_STATUS_CHANGE,
    ORDER_STATUS_UPDATE_ERROR,
    ORDER_STATUS_UPDATE_START,
    ORDER_STATUS_UPDATE_SUCCESS,
    ORDER_UPDATE_ERROR,
    ORDER_UPDATE_SUCCESS,
    PAYMENT_METHOD_INPUT_CHANGE,
    PHONE_INPUT_CHANGE,
    REGISTER_ADDRESS_CHANGE,
    REGISTER_CITY_CHANGE,
    REGISTER_DISTRICT_CHANGE,
    REGISTER_EMAIL_CHANGE,
    REGISTER_FULLNAME_CHANGE,
    REGISTER_PASSWORD_CHANGE,
    REGISTER_PHONE_CHANGE,
    REGISTER_REPEAT_PASSWORD_CHANGE,
    REGISTER_USERNAME_CHANGE,
    REGISTER_WARD_CHANGE,
    USER_AUTH_ERROR,
    USER_LOGIN_SUCCESS,
    USER_LOGOUT_SUCCESS,
    USER_REGISTER_FALSE,
    USER_REGISTER_TRUE,
    VOUCHER_INPUT_CHANGE,
    WARD_INPUT_CHANGE
} from "../constants/user.const"
const addressList = require("../vietnamAddress.json");

export const loginSuccessAction = (userData) => {
    // Lưu thông tin user vào session storage
    sessionStorage.setItem("user", JSON.stringify(userData));
    return {
        type: USER_LOGIN_SUCCESS,
        payload: userData
    }
}

export const logoutSuccessAction = (axiosJWT) => async (dispatch) => {
    // get user data:
    let userData = JSON.parse(sessionStorage.getItem("user"));
    // set up axios option:
    const axiosOption = {
        withCredentials: true,
        method: "POST",
        url: "http://localhost:8000/logout/",
        headers: {
            "Token": `Bearer ${userData.accessToken}`
        },
    };
    // call api logout:
    await axiosJWT.request(axiosOption);
    // delete user infor from session storage
    sessionStorage.removeItem("user");
    return dispatch({
        type: USER_LOGOUT_SUCCESS,
    });
}

export const authUserErrorAction = (error) => {
    return {
        type: USER_AUTH_ERROR,
        payload: error
    }
}

export const registerUsernameChangeAction = (value) => {
    return {
        type: REGISTER_USERNAME_CHANGE,
        payload: value
    }
}

export const registerPasswordChangeAction = (value) => {
    return {
        type: REGISTER_PASSWORD_CHANGE,
        payload: value
    }
}

export const registerRepeatPasswordChangeAction = (value) => {
    return {
        type: REGISTER_REPEAT_PASSWORD_CHANGE,
        payload: value
    }
}

export const registerFullnameChangeAction = (value) => {
    return {
        type: REGISTER_FULLNAME_CHANGE,
        payload: value
    }
}

export const registerEmailChangeAction = (value) => {
    return {
        type: REGISTER_EMAIL_CHANGE,
        payload: value
    }
}

export const registerPhoneChangeAction = (value) => {
    return {
        type: REGISTER_PHONE_CHANGE,
        payload: value
    }
}

export const registerAddressChangeAction = (value) => {
    return {
        type: REGISTER_ADDRESS_CHANGE,
        payload: value
    }
}

export const registerCityChangeAction = (city, districtList) => {
    return {
        type: REGISTER_CITY_CHANGE,
        payload: {
            city,
            districtList
        }
    }
}

export const registerDistrictChangeAction = (district, wardList) => {
    return {
        type: REGISTER_DISTRICT_CHANGE,
        payload: {
            district,
            wardList
        }
    }
}

export const registerWardChangeAction = (ward) => {
    return {
        type: REGISTER_WARD_CHANGE,
        payload: ward
    }
}

export const checkRegisterAndCreateUserAction = () => {
    return async (dispatch, getState) => {
        await dispatch({
            type: CHECK_REGISTER_DATA
        })
        // get current state value of userReducer after above check dispatch:
        const { userReducer } = getState();
        if (userReducer.registerValid) {
            await dispatch({
                type: CREATE_USER_API_START
            });
            const registerData = {
                username: userReducer.registerData.username,
                password: userReducer.registerData.password,
                fullName: userReducer.registerData.fullName,
                phone: userReducer.registerData.phone,
                email: userReducer.registerData.email,
                address: userReducer.registerData.address,
                city: userReducer.registerData.city,
                district: userReducer.registerData.district,
                ward: userReducer.registerData.ward,
            };
            const registerURL = `http://localhost:8000/customers/`;
            const registerFetchOption = {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify(registerData)
            };

            const registerRes = await fetch(registerURL, registerFetchOption);
            const newCustomer = await registerRes.json();

            if (registerRes.status !== 201) {
                return dispatch({
                    type: CREATE_USER_SERVER_ERROR,
                    payload: newCustomer.message
                });
            }
            else {
                return dispatch({
                    type: CREATE_USER_API_SUCCESS
                });
            }
        }
    }
}

export const getUserInforAction = (userData) => {
    return async dispatch => {
        const res = await fetch(`http://localhost:8000/customers/${userData.customerId}`);
        const data = await res.json();
        const payloadData = {
            fullName: data.fullName,
            phone: data.phone,
            email: data.email,
        }
        return dispatch({
            type: GET_USER_INFORMATION,
            payload: payloadData
        })
    }
}

export const getUserAddressAction = (userData) => {
    return async dispatch => {
        const res = await fetch(`http://localhost:8000/customers/${userData.customerId}`);
        const data = await res.json();
        const districtList = addressList.filter(element => element.name === data.city)[0].districts;
        const wardList = districtList.filter(element => element.name === data.district)[0].wards;
        const payloadData = {
            address: data.address,
            city: data.city,
            district: data.district,
            ward: data.ward,
            districtList,
            wardList
        }
        return dispatch({
            type: GET_USER_ADDRESS,
            payload: payloadData
        })
    }
}

export const nameInputChangeAction = (value) => {
    return {
        type: NAME_INPUT_CHANGE,
        payload: value
    }
}

export const phoneInputChangeAction = (value) => {
    return {
        type: PHONE_INPUT_CHANGE,
        payload: value
    }
}

export const emailInputChangeAction = (value) => {
    return {
        type: EMAIL_INPUT_CHANGE,
        payload: value
    }
}

export const addressInputChangeAction = (value) => {
    return {
        type: ADDRESS_INPUT_CHANGE,
        payload: value
    }
}

export const cityInputChangeAction = (city, districtList) => {
    return {
        type: CITY_INPUT_CHANGE,
        payload: {
            city,
            districtList
        }
    }
}

export const districtInputChangeAction = (district, wardList) => {
    return {
        type: DISTRICT_INPUT_CHANGE,
        payload: {
            district,
            wardList
        }
    }
}

export const wardInputChangeAction = (value) => {
    return {
        type: WARD_INPUT_CHANGE,
        payload: value
    }
}

export const messageInputChangeAction = (value) => {
    return {
        type: MESSAGE_INPUT_CHANGE,
        payload: value
    }
}

export const voucherInputChangeAction = (value) => {
    return {
        type: VOUCHER_INPUT_CHANGE,
        payload: value
    }
}

export const paymentMethodInputChangeAction = (value) => {
    return {
        type: PAYMENT_METHOD_INPUT_CHANGE,
        payload: value
    }
}

export const checkInfoAndCreateOrderAction = (userData, cartProducts, axiosJWT) => async (dispatch, getState) => {
    try {
        // check billing information:
        await dispatch({
            type: CHECK_ORDER_INFORMATION
        });
        // get current state value of userReducer after above check dispatch:
        const { userReducer } = getState();

        // if orderValid = true: Start call api to create new order
        if (userReducer.orderValid) {
            await dispatch({
                type: CREATE_ORDER_API_START
            });
            // get create new order data
            const createOrderData = {
                fullName: userReducer.orderInformation.fullName,
                phone: userReducer.orderInformation.phone,
                email: userReducer.orderInformation.email,
                address: userReducer.orderInformation.address,
                city: userReducer.orderInformation.city,
                district: userReducer.orderInformation.district,
                ward: userReducer.orderInformation.ward,
                note: userReducer.orderInformation.note
            }
            // get create new order api url
            const createOrderURL = `http://localhost:8000/customers/${userData.customerId}/orders`;
            // set create new order headers. apply access token to headers
            const createOrderAxiosOption = {
                headers: {
                    "Token": `Bearer ${userData.accessToken}`
                },
            }
            // start calling api
            const createOrderRes = await axiosJWT.post(createOrderURL, createOrderData, createOrderAxiosOption);
            // if not success: stop and return error (status !== 201): 
            if (createOrderRes.status !== 201) {
                return dispatch({
                    type: CREATE_ORDER_CLIENT_ERROR,
                    payload: createOrderRes.data
                })
            }

            // if create new order success: Call API create order detail of new order:
            for await (let product of cartProducts) {
                const orderDetailURL = `http://localhost:8000/orders/${createOrderRes.data._id}/details`;
                const orderDetailData = {
                    product: product._id,
                    quantity: product.quantity
                };
                const orderDetailAxiosOption = {
                    headers: {
                        "Token": `Bearer ${userData.accessToken}`
                    },
                };
                await axiosJWT.post(orderDetailURL, orderDetailData, orderDetailAxiosOption);
            }
            // remove data of cart on local storage
            localStorage.removeItem("cart");
            // update state cartProduct = 0
            await dispatch({
                type: PRODUCT_CART_UPDATE,
                payload: 0
            })
            return dispatch({
                type: CREATE_ORDER_API_SUCCESS,
                payload: createOrderRes.data.orderCode
            })
        }
    }
    catch (err) {
        console.log(err)
        return dispatch({
            type: CREATE_ORDER_API_ERROR,
            payload: err
        })
    }
}

export const openLoginRequireModalAction = () => {
    return {
        type: OPEN_LOGIN_REQUIRE_MODAL
    }
}

export const closeLoginRequireModalAction = () => {
    return {
        type: CLOSE_LOGIN_REQUIRE_MODAL
    }
}

export const userRegisterTrueAction = () => {
    return {
        type: USER_REGISTER_TRUE
    }
}

export const userRegisterFalseAction = () => {
    return {
        type: USER_REGISTER_FALSE
    }
}

export const btnLoginClickAction = (user, navigate) => {
    return async (dispatch) => {
        try {
            const res = await axios.post("http://localhost:8000/login", user, { withCredentials: true });
            const data = res.data;

            // lưu user kèm token vào session storage
            sessionStorage.setItem("user", JSON.stringify(data));
            navigate("/");
            return dispatch({
                type: USER_LOGIN_SUCCESS,
                payload: data
            })

        } catch (err) {
            return dispatch({
                type: LOGIN_ERROR,
                payload: err.response.data
            })
        }

    }
}

export const closeUserSuccessAlertAction = () => {
    return {
        type: CLOSE_SUCCESS_ALERT
    }
}

export const closeUserErrorAlertAction = () => {
    return {
        type: CLOSE_ERROR_ALERT
    }
}

export const createNewCheckoutAction = () => {
    return {
        type: CREATE_NEW_CHECKOUT
    }
}

export const getAllOrdersAction = (userData, axiosJWT, limit, currentPage) => async (dispatch) => {
    try {
        await dispatch({
            type: GET_ALL_ORDERS_START
        });
        // let userData = JSON.parse(sessionStorage.getItem("user")) || [];
        // set URL api get all orders:
        const getAllOrdersURL = "http://localhost:8000/orders";
        // apply access token to headers
        const axiosOption = {
            headers: {
                "Token": `Bearer ${userData.accessToken}`
            },
        };
        // start call api
        const allOrdersRes = await axiosJWT.get(getAllOrdersURL, axiosOption);

        // get total order
        const totalOrders = allOrdersRes.data.length;
        // calculate total pages for pagination:
        const totalPages = Math.ceil(totalOrders / limit);

        // get start index:
        const skipValue = (currentPage - 1) * limit;
        // set up param:
        const getOrderPaginationURL = `http://localhost:8000/orders?skip=${skipValue}&limit=${limit}`;
        const res = await axiosJWT.get(getOrderPaginationURL, axiosOption);

        // return data to state
        return dispatch({
            type: GET_ALL_ORDERS_SUCCESS,
            payload: {
                totalPages,
                data: res.data
            }
        })
    }
    catch (err) {
        console.log(err)
        return dispatch({
            type: GET_ALL_ORDERS_ERROR
        })
    }
}

export const orderDetailOpenAction = (order, userData, axiosJWT) => async (dispatch) => {
    try {
        // dispatch state open order detail modal
        await dispatch({
            type: ORDER_DETAIL_MODAL_OPEN
        });
        // dispatch state loading
        await dispatch({
            type: GET_ORDER_DETAIL_START
        });

        const getOrderDetailURL = `http://localhost:8000/orders/${order._id}/details`
        const axiosOption = {
            headers: {
                "Token": `Bearer ${userData.accessToken}`
            },
        };
        const { data } = await axiosJWT.get(getOrderDetailURL, axiosOption);

        // dispatch state order detail data
        return dispatch({
            type: GET_ORDER_DETAIL_SUCCESS,
            payload: data
        });
    }
    catch (err) {
        return dispatch({
            type: GET_ORDER_DETAIL_ERROR
        });
    }
}

export const orderDetailCloseAction = () => {
    return {
        type: ORDER_DETAIL_MODAL_CLOSE
    }
}

export const btnDeleteOrderClickAction = (orderId) => {
    return {
        type: DELETE_ORDER_MODAL_OPEN,
        payload: orderId
    }
}

export const deleteOrderModalCloseAction = () => {
    return {
        type: DELETE_ORDER_MODAL_CLOSE
    }
}

// action click button create order on page admin product management
export const btnCreateOrderClickAction = () => {
    return {
        type: CREATE_ORDER_MODAL_OPEN
    }
}

// action close modal create order on page admin product management
export const closeCreateOrderModalAction = () => {
    return {
        type: CREATE_ORDER_MODAL_CLOSE
    }
}

export const orderPaginationChangeAction = (value) => {
    return {
        type: ORDER_ADMIN_PAGINATION_CHANGE,
        payload: value
    }
}

export const orderStatusChangeAction = (value) => {
    return {
        type: ORDER_STATUS_CHANGE,
        payload: value
    }
}

export const orderNoteChangeAction = (value) => {
    return {
        type: ORDER_NOTE_CHANGE,
        payload: value
    }
}

export const statusUpdateHandlerAction = (orderStatus, orderId, axiosJWT) => async (dispatch) => {
    try {
        await dispatch({
            type: ORDER_STATUS_UPDATE_START
        });
        let userData = JSON.parse(sessionStorage.getItem("user")) || [];
        // set URL api get all orders:
        const updateOrderURL = `http://localhost:8000/orders/${orderId}`;
        // apply access token to headers
        const axiosOption = {
            headers: {
                "Token": `Bearer ${userData.accessToken}`
            },
        };
        if (orderStatus === "Delivered") {
            // set update data:
            const axiosData = {
                status: orderStatus,
                shippedDate: new Date().toLocaleString("en-gb")
            };
            await axiosJWT.put(updateOrderURL, axiosData, axiosOption);
        }
        else if (orderStatus === "Canceled") {
            // set update data:
            const axiosData = {
                status: orderStatus,
                shippedDate: "Đơn hủy"
            };
            await axiosJWT.put(updateOrderURL, axiosData, axiosOption);
        }
        else {
            // set update data:
            const axiosData = {
                status: orderStatus,
            };
            await axiosJWT.put(updateOrderURL, axiosData, axiosOption);
        }

        // dispatch update status order success
        return dispatch({
            type: ORDER_STATUS_UPDATE_SUCCESS,
            payload: orderStatus
        })
    }
    catch (err) {
        return dispatch({
            type: ORDER_STATUS_UPDATE_ERROR
        })
    }
}

export const orderDeleteConfirmAction = (userData, orderId, axiosJWT) => async (dispatch) => {
    try {
        await dispatch({
            type: ORDER_DELETE_START
        });
        const deleteOrderURL = `http://localhost:8000/orders/${orderId}`;
        // apply access token to headers
        const axiosOption = {
            headers: {
                "Token": `Bearer ${userData.accessToken}`
            },
        };
        // start call api to delete order:
        await axiosJWT.delete(deleteOrderURL, axiosOption);

        return dispatch({
            type: ORDER_DELETE_SUCCESS
        })
    }
    catch (err) {
        return dispatch({
            type: ORDER_DELETE_ERROR
        })
    }
}

export const updateOrderHandlerAction = (note, orderId, axiosJWT) => async (dispatch) => {
    try {
        await dispatch({
            type: ORDER_STATUS_UPDATE_START
        });
        let userData = JSON.parse(sessionStorage.getItem("user")) || [];
        // set URL api get all orders:
        const updateOrderURL = `http://localhost:8000/orders/${orderId}`;
        // apply access token to headers
        const axiosOption = {
            headers: {
                "Token": `Bearer ${userData.accessToken}`
            },
        };
        // set update order data:
        const axiosData = {
            note: note
        };
        // start call api update order
        await axiosJWT.put(updateOrderURL, axiosData, axiosOption);

        // dispatch update status order success
        return dispatch({
            type: ORDER_UPDATE_SUCCESS,
        })
    }
    catch (err) {
        return dispatch({
            type: ORDER_UPDATE_ERROR
        })
    }
}

// action for admin create order input change:
export const adminUsernameChangeAction = (value) => {
    return {
        type: ADMIN_CREATE_ORDER_USERNAME_CHANGE,
        payload: value
    }
}
export const adminFullnameChangeAction = (value) => {
    return {
        type: ADMIN_CREATE_ORDER_NAME_CHANGE,
        payload: value
    }
}
export const adminEmailChangeAction = (value) => {
    return {
        type: ADMIN_CREATE_ORDER_EMAIL_CHANGE,
        payload: value
    }
}
export const adminPhoneChangeAction = (value) => {
    return {
        type: ADMIN_CREATE_ORDER_PHONE_CHANGE,
        payload: value
    }
}
export const adminAddressChangeAction = (value) => {
    return {
        type: ADMIN_CREATE_ORDER_ADDRESS_CHANGE,
        payload: value
    }
}
export const adminCityChangeAction = (city, districtList) => {
    return {
        type: ADMIN_CREATE_ORDER_CITY_CHANGE,
        payload: {
            city,
            districtList
        }
    }
}
export const adminDistrictChangeAction = (district, wardList) => {
    return {
        type: ADMIN_CREATE_ORDER_DISTRICT_CHANGE,
        payload: {
            district,
            wardList
        }
    }
}
export const adminWardChangeAction = (value) => {
    return {
        type: ADMIN_CREATE_ORDER_WARD_CHANGE,
        payload: value
    }
}
export const adminNoteChangeAction = (value) => {
    return {
        type: ADMIN_CREATE_ORDER_MESSAGE_CHANGE,
        payload: value
    }
}

export const adminCreateOrderRestFormAction = () => {
    return {
        type: ADMIN_CREATE_ORDER_RESET_FORM
    }
}

// get customer infor by username action:
export const getUserInforByUsernameAction = (userData, username, axiosJWT) => async (dispatch) => {
    try {
        if (!username) {
            return dispatch({
                type: GET_USER_INFOR_USERNAME_EMPTY
            })
        }
        else {
            await dispatch({
                type: GET_USER_INFOR_BY_USERNAME_START
            });
            // set URL api get customer info by username:
            const getUserInfoURL = `http://localhost:8000/admin?username=${username}`;
            // apply access token to headers
            const axiosOption = {
                headers: { "Token": `Bearer ${userData.accessToken}` },
            };
            // start call api update order
            const { data } = await axiosJWT.get(getUserInfoURL, axiosOption);

            // get district and ward list
            const districtList = addressList.filter(element => element.name === data.city)[0].districts;
            const wardList = districtList.filter(element => element.name === data.district)[0].wards;

            // set payload data
            const payloadData = {
                customerId: data._id,
                email: data.email,
                fullName: data.fullName,
                phone: data.phone,
                address: data.address,
                city: data.city,
                district: data.district,
                ward: data.ward,
                districtList,
                wardList
            };

            // dispatch get user info by username success
            return dispatch({
                type: GET_USER_INFOR_BY_USERNAME_SUCCESS,
                payload: payloadData
            });
        }
    }
    catch (err) {
        return dispatch({
            type: GET_USER_INFOR_BY_USERNAME_ERROR
        })
    }
}

// get customer infor by email action:
export const getUserInforByEmailAction = (userData, email, axiosJWT) => async (dispatch) => {
    try {
        if (!email) {
            return dispatch({
                type: GET_USER_INFOR_EMAIL_EMPTY
            })
        }
        else {
            await dispatch({
                type: GET_USER_INFOR_BY_EMAIL_START
            });
            // set URL api get customer info by email:
            const getUserInfoURL = `http://localhost:8000/admin?email=${email}`;
            // apply access token to headers
            const axiosOption = {
                headers: { "Token": `Bearer ${userData.accessToken}` },
            };
            // start call api update order
            const { data } = await axiosJWT.get(getUserInfoURL, axiosOption);

            // get district and ward list
            const districtList = addressList.filter(element => element.name === data.city)[0].districts;
            const wardList = districtList.filter(element => element.name === data.district)[0].wards;

            // set payload data
            const payloadData = {
                customerId: data._id,
                username: data.user.username,
                fullName: data.fullName,
                phone: data.phone,
                address: data.address,
                city: data.city,
                district: data.district,
                ward: data.ward,
                districtList,
                wardList
            };

            // dispatch get user info by username success
            return dispatch({
                type: GET_USER_INFOR_BY_EMAIL_SUCCESS,
                payload: payloadData
            });
        }
    }
    catch (err) {
        return dispatch({
            type: GET_USER_INFOR_BY_EMAIL_ERROR
        })
    }
}

// get customer infor by phone action:
export const getUserInforByPhoneAction = (userData, phone, axiosJWT) => async (dispatch) => {
    try {
        if (!phone) {
            return dispatch({
                type: GET_USER_INFOR_PHONE_EMPTY
            })
        }
        else {
            await dispatch({
                type: GET_USER_INFOR_BY_PHONE_START
            });
            // set URL api get customer info by phone:
            const getUserInfoURL = `http://localhost:8000/admin?phone=${phone}`;
            // apply access token to headers
            const axiosOption = {
                headers: { "Token": `Bearer ${userData.accessToken}` },
            };
            // start call api update order
            const { data } = await axiosJWT.get(getUserInfoURL, axiosOption);

            // get district and ward list
            const districtList = addressList.filter(element => element.name === data.city)[0].districts;
            const wardList = districtList.filter(element => element.name === data.district)[0].wards;

            // set payload data
            const payloadData = {
                customerId: data._id,
                username: data.user.username,
                email: data.email,
                fullName: data.fullName,
                address: data.address,
                city: data.city,
                district: data.district,
                ward: data.ward,
                districtList,
                wardList
            };

            // dispatch get user info by username success
            return dispatch({
                type: GET_USER_INFOR_BY_PHONE_SUCCESS,
                payload: payloadData
            });
        }
    }
    catch (err) {
        return dispatch({
            type: GET_USER_INFOR_BY_PHONE_ERROR
        })
    }
}

// add product to admin cart action:
export const addProductToAdminCartAction = (adminCartProduct, cartData) => {
    // seach if product exist in cart:
    const index = adminCartProduct.findIndex(element => element.name === cartData.name);
    // if product exist (index not equal -1): increase quantity of exist product by new cart data quantity
    if (index !== -1) {
        adminCartProduct[index].quantity += cartData.quantity;
    }
    // if product not exist: push new cart data to admin cart
    else {
        adminCartProduct.push(cartData);
    }
    return {
        type: ADD_PRODUCT_TO_ADMIN_CART,
        payload: adminCartProduct
    }
}

// add product to admin cart with empty name action:
export const addProductToAdminCartEmptyNameAction = () => {
    return {
        type: ADD_PRODUCT_TO_ADMIN_CART_EMPTY_NAME
    }
}

// increase product quantity in admin cart action
export const increaseProductQuantityAction = (adminCartProduct, index) => {
    adminCartProduct[index].quantity += 1;
    return {
        type: INCREASE_PRODUCT_QUANTITY,
        payload: adminCartProduct
    }
}

// decrease product quantity in admin cart action
export const decreaseProductQuantityAction = (adminCartProduct, index) => {
    adminCartProduct[index].quantity -= 1;
    return {
        type: DECREASE_PRODUCT_QUANTITY,
        payload: adminCartProduct
    }
}

// check admin create order info and call api create new order action:
export const checkAdminOrderInfoAndCreateOrderAction = (userData, cartProducts, axiosJWT) => async (dispatch, getState) => {
    try {
        // check customer information:
        await dispatch({
            type: ADMIN_CHECK_ORDER_INFORMATION
        });
        // get current state value of userReducer after above check dispatch:
        const { userReducer } = getState();

        // if orderValid = true: Start call api to create new order
        if (userReducer.adminOrderValid) {
            await dispatch({
                type: ADMIN_CREATE_ORDER_START
            });
            // get create new order data
            const createOrderData = {
                fullName: userReducer.adminCreateOrderInfor.fullName,
                phone: userReducer.adminCreateOrderInfor.phone,
                email: userReducer.adminCreateOrderInfor.email,
                address: userReducer.adminCreateOrderInfor.address,
                city: userReducer.adminCreateOrderInfor.city,
                district: userReducer.adminCreateOrderInfor.district,
                ward: userReducer.adminCreateOrderInfor.ward,
                note: userReducer.adminCreateOrderInfor.note
            }
            // set create new order headers. apply access token to headers
            const axiosOption = {
                headers: {
                    "Token": `Bearer ${userData.accessToken}`
                },
            }
            // get create new order api url
            const createOrderURL = `http://localhost:8000/customers/${userReducer.adminCreateOrderInfor.customerId}/orders`;

            // start calling api create order of selected customer
            const createOrderRes = await axiosJWT.post(createOrderURL, createOrderData, axiosOption);

            // if create new order success: Call API create order detail of new order:
            for await (let product of cartProducts) {
                const orderDetailURL = `http://localhost:8000/orders/${createOrderRes.data._id}/details`;
                const orderDetailData = {
                    product: product.id,
                    quantity: product.quantity
                };
                // call api create order detail
                await axiosJWT.post(orderDetailURL, orderDetailData, axiosOption);
            }
            // dispatch create order success
            await dispatch({
                type: ADMIN_CREATE_ORDER_SUCCESS,
            });
            // delete all data in create order modal form
            return dispatch({
                type: ADMIN_CREATE_ORDER_RESET_FORM
            })
        }
    }
    catch (err) {
        console.log(err);
        return dispatch({
            type: ADMIN_CREATE_ORDER_ERROR,
            payload: err
        })
    }
}

// 2. Customer Action
// admin get all customers action
export const getAllCustomersAction = (userData, axiosJWT, limit, currentPage) => async (dispatch) => {
    try {
        await dispatch({
            type: GET_ALL_CUSTOMERS_START
        });
        // set up header with jwt
        const axiosOption = {
            headers: { "Token": `Bearer ${userData.accessToken}` }
        };
        // set up URL get all customers
        const getAllCustomersURL = `http://localhost:8000/customers`;
        // start call api get all customers:
        const { data } = await axiosJWT.get(getAllCustomersURL, axiosOption);
        // calculate total number of customers
        const totalCustomers = data.length;
        // calculate total pages for pagination
        const totalPages = Math.ceil(totalCustomers / limit);
        // calculate skip value for pagination
        const skipValue = (currentPage - 1) * limit;

        // set up URL with pagination:
        const customerDataPaginationURL = `http://localhost:8000/customers?skip=${skipValue}&limit=${limit}`;
        // start call api
        const res = await axiosJWT.get(customerDataPaginationURL, axiosOption);

        // dispatch success get data to reducer
        return dispatch({
            type: GET_ALL_CUSTOMERS_SUCCESS,
            payload: {
                allCustomers: res.data,
                totalPages,
            }
        })
    }
    catch (err) {
        return dispatch({
            type: GET_ALL_CUSTOMERS_ERROR,
            payload: err
        })
    }
}

export const adminCustomerPaginationChangeAction = (value) => {
    return {
        type: CUSTOMER_ADMIN_PAGINATION_CHANGE,
        payload: value
    }
}

// admin open customer detail modal action
export const closeCustomerDetailModalAction = () => {
    return {
        type: CUSTOMER_DETAIL_MODAL_CLOSE,
    }
}

// admin get customer detail action:
export const getCustomerDetailAction = (userData, customerId, axiosJWT) => async (dispatch) => {
    try {
        await dispatch({
            type: CUSTOMER_DETAIL_MODAL_OPEN,
            payload: customerId
        })
        // loading data screen
        await dispatch({
            type: GET_CUSTOMER_DETAIL_START
        });
        // set up header with jwt
        const axiosOption = {
            headers: { "Token": `Bearer ${userData.accessToken}` }
        };
        // set up URL get customer detail
        const getAllCustomersURL = `http://localhost:8000/customers/${customerId}`;
        // start call api get all customers:
        const { data } = await axiosJWT.get(getAllCustomersURL, axiosOption);
        // set address list
        const districtList = addressList.filter(element => element.name === data.city)[0].districts;
        const wardList = districtList.filter(element => element.name === data.district)[0].wards;
        // set data to dispatch:
        const customerDetailAddressList = {
            cityList: addressList,
            districtList,
            wardList
        };

        // dispatch success customer detail data to reducer
        return dispatch({
            type: GET_CUSTOMER_DETAIL_SUCCESS,
            payload: {
                customerDetailAddressList,
                customerDetail: data
            }
        })
    }
    catch (err) {
        return dispatch({
            type: GET_CUSTOMER_DETAIL_ERROR
        })
    }
}

// admin update customer detail input change action:
export const customerDetailFullnameChangeAction = (value) => {
    return {
        type: ADMIN_UPDATE_CUSTOMER_FULLNAME_CHANGE,
        payload: value
    }
}
export const customerDetailPhoneChangeAction = (value) => {
    return {
        type: ADMIN_UPDATE_CUSTOMER_PHONE_CHANGE,
        payload: value
    }
}
export const customerDetailAddressChangeAction = (value) => {
    return {
        type: ADMIN_UPDATE_CUSTOMER_ADDRESS_CHANGE,
        payload: value
    }
}
export const customerDetailCityChangeAction = (city, districtList) => {
    return {
        type: ADMIN_UPDATE_CUSTOMER_CITY_CHANGE,
        payload: {
            city,
            districtList
        }
    }
}
export const customerDetailDistrictChangeAction = (district, wardList) => {
    return {
        type: ADMIN_UPDATE_CUSTOMER_DISTRICT_CHANGE,
        payload: {
            district,
            wardList
        }
    }
}
export const customerDetailWardChangeAction = (value) => {
    return {
        type: ADMIN_UPDATE_CUSTOMER_WARD_CHANGE,
        payload: value
    }
}

export const adminUpdateCustomerAction = (userData, customerId, axiosJWT) => async (dispatch, getState) => {
    try {
        await dispatch({
            type: CHECK_CUSTOMER_UPDATE
        });
        // get current state value of userReducer after above check dispatch:
        const { userReducer } = getState();
        // if updateCustomerValid = true: Start call api to create new order
        if (userReducer.updateCustomerDataValid) {
            await dispatch({
                type: ADMIN_UPDATE_CUSTOMER_START
            });

            // set up header with jwt
            const axiosOption = {
                headers: { "Token": `Bearer ${userData.accessToken}` }
            };
            // set up URL get customer detail
            const updateCustomerURL = `http://localhost:8000/customers/${customerId}`;
            const updateData = {
                fullName: userReducer.customerDetail.fullName,
                email: userReducer.customerDetail.email,
                phone: userReducer.customerDetail.phone,
                address: userReducer.customerDetail.address,
                city: userReducer.customerDetail.city,
                district: userReducer.customerDetail.district,
                ward: userReducer.customerDetail.ward,
            };
            await axiosJWT.put(updateCustomerURL, updateData, axiosOption);
            return dispatch({
                type: ADMIN_UPDATE_CUSTOMER_SUCCESS
            });
        }
    }
    catch (err) {
        return dispatch({
            type: ADMIN_UPDATE_CUSTOMER_ERROR
        });
    }
}

// admin delete customer modal action
export const adminDeleteCustomerModalOpenAction = (customerId) => {
    return {
        type: CUSTOMER_DELETE_MODAL_OPEN,
        payload: customerId
    }
}
export const adminDeleteCustomerModalCloseAction = () => {
    return {
        type: CUSTOMER_DELETE_MODAL_CLOSE
    }
}

// admin delete customer action
export const adminDeleteCustomerAction = (userData, customerId, axiosJWT) => async (dispatch) => {
    try {

    }
    catch (err) {

    }
}
// admin open customer order modal action:
export const customerOrderModalOpenAction = (allOrders, userData, axiosJWT) => async (dispatch) => {
    try {
        await dispatch({
            type: CUSTOMER_ORDER_MODAL_OPEN,
            payload: allOrders
        });
        await dispatch({
            type: ADMIN_GET_ORDER_DETAIL_OF_CUSTOMER_START
        });
        // get order detail of all order:
        // set up header with jwt
        const axiosOption = {
            headers: { "Token": `Bearer ${userData.accessToken}` }
        };
        let orderPayloadData = [];
        for (let order of allOrders) {
            // set up payload data
            let orderData = {
                orderCode: order.orderCode,
                orderDate: order.orderDate,
                shippedDate: order.shippedDate,
                fullName: order.fullName,
                email: order.email,
                phone: order.phone,
                address: order.address,
                city: order.city,
                district: order.district,
                ward: order.ward,
                note: order.note,
                status: order.status,
                totalPrice: 0,
                orderDetails: []
            };

            // get detail of all order details
            for await (let orderDetail of order.orderDetails) {
                // get order detail of orders:
                const getOrderDetailURL = `http://localhost:8000/details/${orderDetail}`;
                const { data } = await axiosJWT.get(getOrderDetailURL, axiosOption);
                orderData.orderDetails.push({
                    name: data.product.name,
                    quantity: data.quantity,
                    price: data.product.promotionPrice
                });
                orderData.totalPrice += (data.quantity * data.product.promotionPrice);
            }
            orderPayloadData.push(orderData);
        }
        return dispatch({
            type: ADMIN_GET_ORDER_DETAIL_OF_CUSTOMER_SUCCESS,
            payload: orderPayloadData
        })
    }
    catch (err) {
        console.log(err)
        return dispatch({
            type: ADMIN_GET_ORDER_DETAIL_OF_CUSTOMER_ERROR
        })
    }
}

// admin close customer order modal action:
export const customerOrderModalCloseAction = () => {
    return {
        type: CUSTOMER_ORDER_MODAL_CLOSE
    }
}