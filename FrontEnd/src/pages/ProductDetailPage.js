import { Container } from "@mui/material";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import BreadCrumb from "../components/BreadCrumb/BreadCrumb";
import ProductInfo from "../components/product-detail/ProductInfo";
import ProductArticle from "../components/product-detail/ProductArticle";
import RelatedProduct from "../components/product-detail/RelatedProduct";
import HeaderComponent from "../components/header/HeaderComponent";
import FooterComponent from "../components/footer/FooterComponent";

const ProductDetailPage = () => {
    const { productId } = useParams();
    const [productDetail, setProductDetail] = useState(null);

    useEffect(() => {
        const getProductDetail = async () => {
            const response = await fetch("http://127.0.0.1:8000/products/" + productId);
            const data = await response.json();
            setProductDetail(data.data);
        };
        getProductDetail();
    }, [productId]);

    const breadcrumbsAddress = [
        { name: "Trang Chủ", url: "/" },
        { name: "Sản Phẩm", url: "/products" },
        { name: productDetail ? productDetail.name : null, url: `/products/${productId}` }
    ]

    return (
        <div>
            <HeaderComponent />
            <Container sx={{ minHeight: 560, py: 3 }}>
                <BreadCrumb address={breadcrumbsAddress} />
                {productDetail
                    ?
                    <div>
                        <ProductInfo productDetail={productDetail} />
                        <ProductArticle productDetail={productDetail} />
                        <RelatedProduct productDetail={productDetail} />
                    </div>
                    :
                    null}
            </Container>
            <FooterComponent />
        </div>

    )
}

export default ProductDetailPage;