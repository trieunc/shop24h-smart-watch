import { Container, Grid } from "@mui/material";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getProductDataAction } from "../actions/product.action";
import BreadCrumb from "../components/BreadCrumb/BreadCrumb";
import ProductFilter from "../components/products/ProductFilter";
import ProductPagination from "../components/products/ProductPagination";
import ProductCard3 from "../components/products/ProductCard3";
import HeaderComponent from "../components/header/HeaderComponent";
import FooterComponent from "../components/footer/FooterComponent";


const ProductPage = () => {
    const { productFilterData, currentPage, limit, filterName, filterMinPrice, filterMaxPrice, filterBrand } = useSelector(reduxData => reduxData.productReducer);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getProductDataAction(currentPage, limit, filterName, filterMinPrice, filterMaxPrice, filterBrand));
        // eslint-disable-next-line
    }, [currentPage, limit]);

    const breadcrumbAddress = [
        { name: "Trang Chủ", url: "/" },
        { name: "Sản Phẩm", url: "/products" },
    ]
    return (
        <div>
            <HeaderComponent />
            <Container sx={{ minHeight: 560, py: 3 }}>
                <BreadCrumb address={breadcrumbAddress} />
                <Grid container spacing={0} mt={6}>
                    <Grid item xs={12} md={3}>
                        <ProductFilter />
                    </Grid>
                    <Grid item sx={{ pl: { md: 3, xs: 0 }, mt: { md: 0, xs: 2 } }} xs={12} md={9}>
                        <Grid container spacing={2}>
                            {productFilterData.map((element, index) => <ProductCard3 key={index} data={element} />)}
                        </Grid>
                    </Grid>
                </Grid>
                <Grid container mt={3} justifyContent={{ xs: "center", md: "end" }}>
                    <Grid item>
                        <ProductPagination />
                    </Grid>
                </Grid>
            </Container>
            <FooterComponent />
        </div>

    )
}

export default ProductPage;