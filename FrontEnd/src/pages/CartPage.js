import { Container } from "@mui/material";
import { useSelector } from "react-redux";
import BreadCrumb from "../components/BreadCrumb/BreadCrumb";
import CartEmpty from "../components/cart/CartEmpty";
import CartTable from "../components/cart/CartTable";
import PaymentButton from "../components/cart/PaymentButton";
import FooterComponent from "../components/footer/FooterComponent";
import HeaderComponent from "../components/header/HeaderComponent";

const CartPage = () => {
    const { cartNumber } = useSelector(reduxData => reduxData.productReducer);

    const breadcrumbsAddress = [
        { name: "Trang Chủ", url: "/" },
        { name: "Giỏ Hàng", url: "/cart" },
    ]

    return (
        <div>
            <HeaderComponent />
            <Container sx={{ minHeight: 560, py: 3 }}>
                <BreadCrumb address={breadcrumbsAddress} />
                {cartNumber !== 0
                    ? <>
                        <CartTable />
                        <PaymentButton />
                    </>
                    : <CartEmpty />
                }
            </Container>
            <FooterComponent />
        </div>
    )
}

export default CartPage;