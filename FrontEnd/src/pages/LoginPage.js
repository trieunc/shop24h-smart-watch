import { Container } from "@mui/material";
import BreadCrumb from "../components/BreadCrumb/BreadCrumb";
import LoginComponent from "../components/login/LoginComponent";
import RegisterComponent from "../components/login/RegisterComponent";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { userRegisterFalseAction } from "../actions/user.action";
import HeaderComponent from "../components/header/HeaderComponent";
import FooterComponent from "../components/footer/FooterComponent";

const LoginPage = () => {
    const { userRegister } = useSelector(reduxData => reduxData.userReducer);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(userRegisterFalseAction());
    }, [dispatch]);

    const breadcrumbsAddress = [
        { name: "Trang Chủ", url: "/" },
        { name: "Đăng Nhập", url: "/login" },
    ]

    return (
        <>
            <HeaderComponent />
            <Container sx={{ minHeight: 560, py: 3 }}>
                <BreadCrumb address={breadcrumbsAddress} />
                {
                    userRegister ? <RegisterComponent /> : <LoginComponent />
                }
            </Container>
            <FooterComponent />
        </>
    )
}

export default LoginPage;