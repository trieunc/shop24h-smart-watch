import { useEffect } from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import SideMenu from "../components/admin/SideMenu";

const AdminPage = () => {
    const {userData} = useSelector(reduxData => reduxData.userReducer);
    const navigate = useNavigate();

    useEffect(() => {
        if (!userData || userData.role !== "admin") {
            navigate("/");
        }
    })

    return (
        <div>
            <SideMenu />
        </div>
    )
}

export default AdminPage;