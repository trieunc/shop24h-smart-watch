import { Container, Grid } from "@mui/material"
import BreadCrumb from "../components/BreadCrumb/BreadCrumb";
import BillingForm from "../components/checkout/BillingForm";
import OrderDetail from "../components/checkout/OrderDetail";
import { useSelector } from "react-redux";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import OrderSuccess from "../components/checkout/OrderSuccess";
import HeaderComponent from "../components/header/HeaderComponent";
import FooterComponent from "../components/footer/FooterComponent";

const CheckoutPage = () => {
    const { userData, orderValid, orderCode } = useSelector(reduxData => reduxData.userReducer);
    const navigate = useNavigate();

    useEffect(() => {
        const cartProduct = JSON.parse(localStorage.getItem("cart")) || [];
        if (cartProduct.length === 0) {
            navigate("/");
        }
        // if not login, back to home page
        if (!userData) {
            navigate("/");
        }
    }, [userData, navigate]);

    const breadcrumbsAddress = [
        { name: "Trang Chủ", url: "/" },
        { name: "Giỏ Hàng", url: "/cart" },
        { name: "Thanh Toán", url: "/checkout" },
    ];

    return (
        <div>
            <HeaderComponent />
            <Container sx={{ minHeight: 560, py: 3 }}>
                <BreadCrumb address={breadcrumbsAddress} />
                {
                    (orderValid && orderCode)
                        ?
                        <OrderSuccess />
                        :
                        <Grid container spacing={2}>
                            <Grid item xs={12} md={5}>
                                <BillingForm />
                            </Grid>
                            <Grid item xs={12} md={7}>
                                <OrderDetail />
                            </Grid>
                        </Grid>
                }
            </Container>
            <FooterComponent />
        </div>

    )
}

export default CheckoutPage;